var upload=angular.module('Spesh');
upload.controller('adminGalleryUploadController',function($scope,$state,$http,$window,$timeout,Upload){
	$scope.buttonName="Save";
    var id='';
    $scope.mulImage=[];
    var galImages=[];
    $scope.mulImage.push({'image':null,'filename':'','comment':''});
    $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':'','comment':''});
	  // console.log('add file',$scope.mulImage);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	   console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
        // if($scope.browser=='Safari'){
             console.log('file',$scope.mulImage);
        // }
   }
   /* $scope.listOfSubcat=[{
		name:'Select Subcategory',
		value:''
	}]
	$scope.subcat=$scope.listOfSubcat[0];*/
    $scope.listOfSubcat=[];
    $http({
        method:'GET',
        url:"php/customerInfo.php?action=getsubcategory",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
       // console.log('res',response.data);
        angular.forEach(response.data,function(obj){
            var data={'name':obj.cat_name+':'+obj.subcat_name,'value':obj.subcat_id};
			$scope.listOfSubcat.push(data);
		})
    },function errorCallback(response){
        
    })
    $http({
        method:'GET',
        url:"php/customerInfo.php?action=getGallery",
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
    }).then(function successCallback(response){
        console.log('res',response.data);
        if(response.data.length >0){
            $scope.listOfGallery=response.data;
        }
        if(response.data=='null'){
            $scope.listOfGallery=[];
           // console.log('res',$scope.listOfGallery);
        }
         //console.log('res',$scope.listOfGallery);
    },function errorCallback(response){
    })
   // console.log('res',$scope.listOfGallery);
    $scope.validateImages=function(){
	   var flag;
	   if($scope.mulImage.length >0){
		   for(var i=0;i<$scope.mulImage.length;i++){
			   if($scope.mulImage[i]['image']==null && $scope.mulImage[i]['filename']==''){
				   alert('Please select Gallery iamge'+(i+1));
				   var flag=false;
				   return;
			   }else{
				   flag=true;
			   }
		  }
		  return flag;
	   }
   }
    $scope.addGalleryImageDetails=function(billdata){
        if(billdata.$valid){
            var flag=true;
            if($scope.subcat==undefined){
                alert('Please select subcategory value');
            }else{
                 flag=$scope.validateImages();
                if(flag==true){
                    if($scope.buttonName=='Save'){
                        if($scope.mulImage.length>0){
						   var imageString='';
						   var arrImage=[];
						   var imagearr=[];
						   if($scope.mulImage[0].image !=null){
							   for(var i=0;i<$scope.mulImage.length;i++){
								   if($scope.mulImage[i]['image']!=null){
										var newmulpath='';
										var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
										newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
										arrImage.push({'image':$scope.mulImage[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }else{
										var newmulpath='';
										newmulpath=$scope.mulImage[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }
							   }//end of for loop
							   if(arrImage.length>0){
								   $scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										method: 'POST',
										file: arrImage
								   }).success(function(data, status, headers, config) {
									   var imageData=$.param({'action':'galleryImagesAdd','subcat_id':$scope.subcat.value,'description':$scope.description,'multiple_image':imageString,'imageArr':imagearr});
									  // console.log('mulImage',imageData);
									   $http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('dashboard.gallery.upload',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
								   }).error(function(data,status){
								   })//end of error statement
							   }//end of third if
						   }//end of second if statement
					   }//end of first if statement
                    }
                    if($scope.buttonName=='Update'){
                        if($scope.mulImage.length>0){
							var imageString='';
						    var arrImage=[];
							var imagearr=[];
							for(var i=0;i<$scope.mulImage.length;i++){
								if($scope.mulImage[i]['image']!=null){
									var newmulpath='';
									var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
									newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
									$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
									arrImage.push({'image':$scope.mulImage[i]['image']});
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}else{
									var newmulpath='';
									newmulpath=$scope.mulImage[i]['filename'];
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}
							}//end of for loop inside update
							//console.log('arrImage',arrImage);
							if(arrImage.length>0){
								$scope.upload=Upload.upload({
									url: 'php/uploadAll.php',
									method: 'POST',
									file: arrImage
								}).success(function(data, status, headers, config) {
									var imageData=$.param({'action':'galleryImagesUpdate','subcat_id':$scope.subcat.value,'description':$scope.description,'multiple_image':imageString,'imageArr':imagearr,'gallery_id':id,'galImages':galImages});
									$http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('dashboard.gallery.upload',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
									
								}).error(function(data,status){
								})
							}else{
								var imageData=$.param({'action':'galleryImagesUpdate','subcat_id':$scope.subcat.value,'description':$scope.description,'multiple_image':imageString,'imageArr':imagearr,'gallery_id':id,'galImages':galImages});
								//console.log('imgData',imageData);
								$http({
									   method:'POST',
									   url:'php/customerInfo.php',
									   data:imageData,
									   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
								   }).then(function successCallback(response){
									   console.log('upres',response.data);
									   alert(response.data['msg']);
									   $state.go('dashboard.gallery.upload',{}, { reload: true });
								   },function errorCallback(response) {
										alert(response.data['msg']);
								   })
							}
						}//end of first if inside update
                    }
                }
            }
        }
    }
    $scope.viewAllImages=function(galleryId){
        var viewData=$.param({'action':'getImage','gallery_id':galleryId});
        $http({
            method:'POST',
            url:'php/customerInfo.php',
            data:viewData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
          //  var str_array = response.data[0].image.split(',');
           // console.log('view',str_array);
            $scope.stepsModel=[];
            for(var i=0;i<response.data.length;i++){
                var data={'image':response.data[i]['image']}
                $scope.stepsModel.push(data);
            }
            $scope.showModal = !$scope.showModal;
        },function errorCallback(response) {
        })
    }
    $scope.editSpecialImageData=function(galleryId){
        id=galleryId;
        var editdata=$.param({'action':'editGallery','gallery_id':id});
        $http({
            method:'POST',
            url:"php/customerInfo.php",
            data:editdata,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
            console.log('edit data',response.data);
            $scope.mulImage=[];
            $scope.mulImage.push({'image':null,'filename':'','comment':''});
            $scope.subcat={};
            $scope.subcat.value=response.data[0].subcat_id;
           // $scope.description=response.data[0].description;
           // var str_array = response.data[0].image.split(',');
            for(var i=0;i<response.data.length;i++){
                if(i==0){
                    $scope.mulImage[i]['filename']=response.data[i].image;
                     $scope.mulImage[i]['comment']=response.data[i]['description'];
                     galImages.push({'images':response.data[i].image});
                }else{
                     $scope.mulImage.push({'image':null,'filename':response.data[i].image,'comment':response.data[i]['description']});
                     galImages.push({'images':response.data[i].image});
                }
            }
            $scope.buttonName="Update";
		    $scope.ClearbuttonName="Cancel";
		    $scope.showCancel=true;
        },function errorCallback(response) {
        })
    }
    $scope.clearImageData=function(){
	    $state.go('dashboard.gallery.upload',{}, { reload: true });
    }
    $scope.deleteProductImageData=function(galleryId){
        id=galleryId;
        var deletedata=$.param({'action':'deleteGallery','gallery_id':id});
        var measg=$window.confirm('Are you sure to delete this ?');
        if(measg){
            $http({
			   method:'POST',
			   url:"php/customerInfo.php",
			   data:deletedata,
			   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		    }).then(function successCallback(response){
			   console.log('res del',response.data);
			   alert(response.data['msg']);
			   $state.go('dashboard.gallery.upload',{}, { reload: true });
		    },function errorCallback(response) {
			   alert(response.data['msg']);
			   $state.go('dashboard.gallery.upload',{}, { reload: true });
		    })
        }
    }
});
upload.directive('modal', function () {
    return {
      template: '<div class="modal fade">' + 
          '<div class="modal-dialog modal-lg">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
      restrict: 'E',
      transclude: true,
      replace:true,
      scope:true,
      link: function postLink(scope, element, attrs) {
        scope.title = attrs.title;

        scope.$watch(attrs.visible, function(value){
          if(value == true)
            $(element).modal('show');
          else
            $(element).modal('hide');
        });

        $(element).on('shown.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = true;
          });
        });

        $(element).on('hidden.bs.modal', function(){
          scope.$apply(function(){
            scope.$parent[attrs.visible] = false;
          });
        });
      }
    };
  });