var customerImage=angular.module('Spesh');
customerImage.controller('businessCustomerImageController',function($scope,$state,$http,$window,$timeout,Upload,focusInputField){
	$scope.buttonName='Add';
	var memb_id='';
	var da_id='';
	var sub_id='';
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=busiImagedisp",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		if(response.data !='null'){
			$scope.listOfSpecialImages=response.data;
		}
		if(response.data =='null'){
			$scope.listOfSpecialImages=[];
		}
	},function errorCallback(response) {
	})
	$scope.listOfRestaurant=[{
		name:'Select Restaurant',
		value:''
	}]
	$scope.restaurant=$scope.listOfRestaurant[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=restaurant",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listOfRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
	$scope.listOfDays=[{
		name:'Select Days',
		value:''
	}]
	$scope.days=$scope.listOfDays[0];
		$http({
		method:'GET',
		url:"php/customerInfo.php?action=buisSpecialDay",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.dayid};
				$scope.listOfDays.push(data);
			})
		},function errorCallback(response) {
		})
	$scope.listOfSubcat=[{
		name:'Select Subcategory',
		value:''
	}]
	$scope.subcat=$scope.listOfSubcat[0];
	$scope.getSubcategoryValue=function(id){
		$scope.listOfSubcat=[];
		$scope.listOfSubcat=[{
			name:'Select Subcategory',
			value:''
	    }]
	    $scope.subcat=$scope.listOfSubcat[0];
		var subcatdata=$.param({'action':'getsubcatbusi','day_id':$scope.days.value});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:subcatdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('day res',response.data);
				angular.forEach(response.data,function(obj){
					var data={'name':obj.subcat_name,'value':obj.subid};
					$scope.listOfSubcat.push(data);
				})
			},function errorCallback(response) {
		})
		
	}
   $scope.mulImage=[];
   $scope.mulImage.push({'image':null,'filename':'','comment':''});
   $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':'','comment':''});
	  // console.log('add file',$scope.mulImage);
	   
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	   console.log('file',$scope.mulImage);
   }
   $scope.onFileSelect1 = function(index) {
	     $scope.mulImage[index]['filename']='';
   }
   $scope.validateImages=function(){
	   var flag;
	   if($scope.mulImage.length >0){
		   for(var i=0;i<$scope.mulImage.length;i++){
			   if($scope.mulImage[i]['image']==null && $scope.mulImage[i]['filename']==''){
				   alert('Please select iamge'+(i+1));
				   var flag=false;
				   return;
			   }else{
				   flag=true;
			   }
		  }
		  return flag;
	   }
   }
   $scope.addImageDetails=function(billdata){
	  // console.log('billdata',billdata.$valid);
	   if(billdata.$valid){
		   var flag=true;
		   if($scope.days.value==null || $scope.days.value==''){
			   alert('Please select day');
			   focusInputField.borderColor('day');
		   }else if($scope.subcat.value==null || $scope.subcat.value==''){
			   alert('Please select sub-category');
			   focusInputField.borderColor('subcat');
		   }else{
			   flag=$scope.validateImages();
			   if(flag==true){
				    if($scope.buttonName=='Add'){
				  // console.log('mulImage',$scope.mulImage.length>0);
						if($scope.mulImage.length>0){
						   var imageString='';
						   var arrImage=[];
						   var imagearr=[];
						   if($scope.mulImage[0].image !=null){
							   for(var i=0;i<$scope.mulImage.length;i++){
								   if($scope.mulImage[i]['image']!=null){
										var newmulpath='';
										var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
										newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
										//$scope.mulImage[i]['image'].name=newmulpath;
										$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
										arrImage.push({'image':$scope.mulImage[i]['image']});
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }else{
										var newmulpath='';
										newmulpath=$scope.mulImage[i]['filename'];
										imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
										if(i==0){
											imageString=newmulpath;
										}else{
											imageString +=","+newmulpath;
										}
								   }
							   }//end of for loop
							   if(arrImage.length>0){
								   $scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										method: 'POST',
										file: arrImage
								   }).success(function(data, status, headers, config) {
									   var imageData=$.param({'action':'imagesAddBusi','day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'add'});
									  // console.log('mulImage',imageData);
									   $http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('business.customer.image',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
								   }).error(function(data,status){
								   })//end of error statement
							   }//end of third if
						   }//end of second if statement
					   }//end of first if statement
			        }//Add if end
					if($scope.buttonName=='Update'){
						//console.log('update',$scope.mulImage.length,$scope.mulImage);
						if($scope.mulImage.length>0){
							var imageString='';
						    var arrImage=[];
							var imagearr=[];
							for(var i=0;i<$scope.mulImage.length;i++){
								if($scope.mulImage[i]['image']!=null){
									var newmulpath='';
									var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
									newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
									$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
									arrImage.push({'image':$scope.mulImage[i]['image']});
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}else{
									var newmulpath='';
									newmulpath=$scope.mulImage[i]['filename'];
									imagearr.push({'imagename':newmulpath,'comnt':$scope.mulImage[i]['comment']});
									if(i==0){
										imageString=newmulpath;
									}else{
										imageString +=","+newmulpath;
									}
								}
							}//end of for loop inside update
							//console.log('arrImage',arrImage);

							if(arrImage.length>0){
								$scope.upload=Upload.upload({
									url: 'php/uploadAll.php',
									method: 'POST',
									file: arrImage
								}).success(function(data, status, headers, config) {
									var imageData=$.param({'action':'imagesAddBusi','day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update'});
									$http({
										   method:'POST',
										   url:'php/customerInfo.php',
										   data:imageData,
										   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									   }).then(function successCallback(response){
										   alert(response.data['msg']);
										   $state.go('business.customer.image',{}, { reload: true });
									   },function errorCallback(response) {
											alert(response.data['msg']);
									   })
									
								}).error(function(data,status){
								})
							}else{
								var imageData=$.param({'action':'imagesAdd','restaurant':$scope.restaurant.value,'day_id':$scope.days.value,'subcat_id':$scope.subcat.value,'multiple_image':imageString,'imageArr':imagearr,'act':'update'});
								//console.log('imgData',imageData);
								$http({
									   method:'POST',
									   url:'php/customerInfo.php',
									   data:imageData,
									   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
								   }).then(function successCallback(response){
									 //  console.log('upres',response.data);
									   alert(response.data['msg']);
									   $state.go('business.customer.image',{}, { reload: true });
								   },function errorCallback(response) {
										alert(response.data['msg']);
								   })
							}
						}//end of first if inside update
					}
			   }
		   }//end of else statement
	   }
   }
   $scope.getDayFromSpecialEdit=function(member_id,dayid){
	    $scope.listOfDays=[];
		$scope.listOfDays=[{
			name:'Select Days',
			value:''
		}]
		$scope.days=$scope.listOfDays[0];
		var daydata=$.param({'action':'specialdays','res_id':member_id});
		$http({
		method:'POST',
		url:"php/customerInfo.php",
		data:daydata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('day res',response.data);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.day_name,'value':obj.dayid};
				$scope.listOfDays.push(data);
			})
			$scope.days.value=dayid;
		},function errorCallback(response) {
		})
   }
   $scope.getSubcategoryValueEdit=function(member_id,dayid,subcatid){
	    $scope.listOfSubcat=[];
		$scope.listOfSubcat=[{
			name:'Select Subcategory',
			value:''
	    }]
	    $scope.subcat=$scope.listOfSubcat[0];
		var subcatdata=$.param({'action':'getsubcatname','member_id':member_id,'day_id':dayid});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:subcatdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('day res',response.data);
				angular.forEach(response.data,function(obj){
					var data={'name':obj.subcat_name,'value':obj.subid};
					$scope.listOfSubcat.push(data);
				})
				$scope.subcat.value=subcatid;
			},function errorCallback(response) {
		})
   }
   $scope.editSpecialImageData=function(memberid,dayid,subcatid){
	   memb_id=memberid;
	   da_id=dayid;
	   sub_id=subcatid;
	   var editImageData=$.param({'action':'editSpecialImage','member_id':memberid,'day_id':dayid,'subcat_id':subcatid});
	   $http({
		   method:'POST',
		   url:"php/customerInfo.php",
		   data:editImageData,
		   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	   }).then(function successCallback(response){
		   $scope.getDayFromSpecialEdit(response.data[0].member_id,response.data[0].day_id);
		   $scope.getSubcategoryValueEdit(response.data[0].member_id,response.data[0].day_id,response.data[0].subcat_id);
		   var multiImage=response.data[0].special_images;
		   var array=multiImage.split(",");
		   for(var i=0;i<array.length;i++){
			   if(i==0){
				 $scope.mulImage[i]['filename']=array[i];
			   }
			   if(i !=0){
				 $scope.mulImage.push({'image':null,'filename':array[i]});
			   }
		   }
		   $scope.buttonName="Update";
		   $scope.ClearbuttonName="Cancel";
		   $scope.showCancel=true;
	   },function errorCallback(response) {
	   })
   }
   $scope.clearImageData=function(){
	    $state.go('business.customer.image',{}, { reload: true });
   }
   $scope.deleteProductImageData=function(memberid,dayid,subcatid){
	   var delImageData=$.param({'action':'delSpecialImage','member_id':memberid,'day_id':dayid,'subcat_id':subcatid});
	   if($window.confirm('Are you want to sure delete this image ?')){
		   $http({
			   method:'POST',
			   url:"php/customerInfo.php",
			   data:delImageData,
			   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		   }).then(function successCallback(response){
			   console.log('res del',response.data);
			   alert(response.data['msg']);
			  $state.go('business.customer.image',{}, { reload: true });
		   },function errorCallback(response) {
			   alert(response.data['msg']);
			   $state.go('business.customer.image',{}, { reload: true });
		   })
	   }
   }
})
customerImage.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
customerImage.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});