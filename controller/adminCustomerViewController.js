var customerView=angular.module('Spesh');
customerView.controller('adminCustomerViewController',function($scope,$state,$http,$window,$timeout,$filter,Upload,focusInputField,browserDetectionforRest){
	//$scope.answers={};
	//$scope.searchProduct = '';
	var fileURL='';
	var id='';
	$scope.currentPage=1;
	$scope.itemsPerPage=5;
	$scope.attchImage1="img/no_image.jpg";
	$scope.imageData='';
	$scope.answers={};
	$scope.selectedAnswers = [];
	/*$scope.listOfCustomerData = [{
    rest_name: "Anjum",
    id:1,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }, {
    rest_name: "A&P Chinese Food Express",
     id:2,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }, {
    rest_name: "Bookers BBQ & Crab Shack",
     id:3,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }, {
    rest_name: "Butcher And The Baker",
     id:4,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }, {
    rest_name: "Cactus Club Stephen Avenue",
     id:5,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }, {
    rest_name: "Cactus Club - Macleod Trail",
     id:6,
     address:"Goa",
     city:"1",
     country:"India",
     date:"2016-07-05 11:39:04",
     image:"",
     member_id:"135",
     mobile:"9937229853",
     person:"Subhrajyoti pradhan",
     postal:"752109",
     premium:"1",
     proviance:"Goa",
     quad_id:"3",
     quadrant:"3",
     quadrant_name:"SW",
     status:"1",
     url:"http://orimark.com"
  }];*/
	/*$http.get('js/customer.json').success(function(data) {
    console.log('data',data);
    $scope.listOfCustomerData=data;
    })*/
	$scope.listRestaurant=[{
		name:'Select Business Name',
		value:''
	}]
    $scope.parentrestname=$scope.listRestaurant[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=rest",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.rest_name,'value':obj.member_id};
			$scope.listRestaurant.push(data);
		})
	},function errorCallback(response) {
	})
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=disp",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		console.log('res',response.data);
		$scope.listOfCustomerData=response.data;
	},function errorCallback(response) {
	})
	/*$scope.listOfSpecial=[{
		name:'Select  Special',
		value:''
	}]
	$scope.special=$scope.listOfSpecial[0];*/
	$scope.example14model = [];
    $scope.example14settings = {
        scrollableHeight: '200px',
        scrollable: true,
        enableSearch: true
    };
    $scope.listOfSpecial=[];
	$scope.example14data = [];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=special",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.special_name,'value':obj.special_id};
			var data1={'label':obj.special_name,'id':obj.special_id};
			$scope.listOfSpecial.push(data);
			$scope.example14data.push(data1);
		})
	},function errorCallback(response) {
	})
	$scope.example2settings = {
	        displayProp: 'id'
	 };
	$scope.listOfCity=[{
		name:'Select  City',
		value:''
	}]
	$scope.citycode=$scope.listOfCity[0];
	$http({
		method:'GET',
		url:"php/customerInfo.php?action=city",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.city_name,'value':obj.city_id};
			$scope.listOfCity.push(data);
		})
	},function errorCallback(response) {
	})
	$scope.listOfSubCatagory = []; 
	$scope.setSubCatagory=function(catid,index,subcat,subcatid){
		$scope.listOfSubCatagory[index]=[];
		var catdata=$.param({'action':'subcat','cat_id':catid});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:catdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.subcat_name,'value':obj.subcat_id};
				$scope.listOfSubCatagory[index].push(data);
			})
			//var data={'subcat':subcat,'subcatid':subcatid};
			//console.log('info',subcat,subcatid);
			$timeout(function(){
			  document.getElementById(subcat).value=subcatid;
			},2000);
		},function errorCallback(response) {
		})
	}
	$scope.listOfSubCatagory1 = []; 
	$scope.setSubCatagory1=function(catid,index,subcat,subcatid){
		$scope.listOfSubCatagory1[index]=[];
		var catdata=$.param({'action':'subcat','cat_id':catid});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:catdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.subcat_name,'value':obj.subcat_id};
				$scope.listOfSubCatagory1[index].push(data);
			})
			//var data={'subcat':subcat,'subcatid':subcatid};
			//console.log('info',subcat,subcatid);
			$timeout(function(){
			  document.getElementById(subcat).value=subcatid;
			},2000);
		},function errorCallback(response) {
		})
	}
	$scope.getCount=function(arr,key,value){
		var count = 0;
        arr.forEach(function(obj){
           if(obj[key] == value){
               count++;
           }
         });
       return count;
	}
	$scope.getQuadrantName=function(quadrant){
		$scope.listOfQuadrant=[];
		$scope.listOfQuadrant=[{
			name:'Select  Quadrant',
			value:''
		}]
		$scope.quadrant=$scope.listOfQuadrant[0];
		$http({
			method:'GET',
			url:"php/customerInfo.php?action=quad",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('hello',response);
			angular.forEach(response.data,function(obj){
				var data={'name':obj.quadrant,'value':obj.quad_id};
				$scope.listOfQuadrant.push(data);
				if(obj.quad_id==quadrant){
					$scope.quadrant.value=quadrant;
				}
			})
			
		},function errorCallback(response) {
		})
	}
	$scope.addNewRow = function(answers,hasDelete) {
    answers.push({
      category: null,
      subcategory: null,
      comment: null,
	  hasDelete: hasDelete ? hasDelete : false
    });
  };
  
  $scope.removeRow = function(answers, index,parent){
    answers.splice(index, 1);
	$scope.listOfSubCategory[parent].splice(index,1);
    //answers.splice(answers.length-1,1);
  };
  $scope.renderWithMode = function(index,catvalue,parent,isEditMode){
    if(isEditMode){
      $scope.removeBorder(index,catvalue,parent);
    }
  };
  $scope.setSubcatag=function(index,childindex,catid,subcat_id,checked){
	 // console.log('hello',index,childindex,catid);
	  var catdata=$.param({'action':'subcat','cat_id':catid});
	  var subcategories=[];
	  $http({
		  method:'POST',
		  url:"php/customerInfo.php",
		  data:catdata,
		  headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	  }).then(function successCallback(res){
		  angular.forEach(res.data,function(obj){
			  var data={'name':obj.subcat_name,'value':obj.subcat_id};
			  if(!$scope.listOfSubCategory[index]){
				  $scope.listOfSubCategory[index] = [];  
			  }
			  subcategories.push(data);
		  })
		  $scope.listOfSubCategory[index][childindex]=subcategories;
		 // console.log('sub',$scope.listOfSubCategory[index][childindex]);
		  $scope.days[index].answers[childindex].subcategory={'value':subcat_id};
		  if(checked==1){
		    $scope.days[index].answers[childindex].check=true;
			var thisAnswer=$scope.days[index].answers[childindex];
			$scope.selectedAnswers[index].chkbox.push(thisAnswer);
		  }
		// console.log('subcat',$scope.days[index].answers);
	  },function errorCallback(response) {
	  });
	  
  }
  $scope.mulImage=[];
   $scope.mulImage.push({'image':null});
   $scope.addNewImageRow=function(mulImage){
	   mulImage.push({'image':null,'filename':''});
   }
   $scope.deleteNewImageRow=function(mulImage,index){
	   mulImage.splice(index,1);
	  // console.log('file',$scope.mulImage[0]['image'].name);
   }
    $scope.setCategoryValue=function(){
       /* if($scope.special.value !='' || $scope.special.value !=null){
            var specdata=$.param({'action':'speccat','speid':$scope.special.value});
            $http({
                method:'POST',
                url:"php/customerInfo.php",
                data:specdata,
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
                console.log('res',response.data);
                $scope.listOfCategory=[];
                angular.forEach(response.data,function(obj){
                    var data={'name':obj.cat_name,'value':obj.cat_id};
                    $scope.listOfCategory.push(data);
                })
            },function errorCallback(response) {
            })
        }*/
       // console.log('example14model 11',$scope.example14model);
        if($scope.example14model.length > 0){
	        var specdata=$.param({'action':'mulspesc','speid':$scope.example14model});
	        $http({
	          method:'POST',
	          url:'php/customerInfo.php',
	          data:specdata,
	          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	        }).then(function successCallback(response){
	          console.log("multi",response.data);
	          $scope.listOfCategory=[];
	          angular.forEach(response.data,function(obj){
	              var data={'name':obj.cat_name,'value':obj.cat_id};
	              $scope.listOfCategory.push(data);
	          })
	        },function errorCallback(response) {
	        })
        }
       // console.log('example14model', $scope.listOfCategory);
    }
    $scope.setMultipleSpecial=function(){
    	if($scope.example14model.length > 0){
	        var specdata=$.param({'action':'mulspesc','speid':$scope.example14model});
	        $http({
	          method:'POST',
	          url:'php/customerInfo.php',
	          data:specdata,
	          headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	        }).then(function successCallback(response){
	          //console.log("multi",response.data);
	          $scope.listOfCategory=[];
	          angular.forEach(response.data,function(obj){
	              var data={'name':obj.cat_name,'value':obj.cat_id};
	              $scope.listOfCategory.push(data);
	          })
	          if($scope.listOfCategory.length >0){
	            alert("Special Type set successfully");
	          }
	          $timeout(function() {
	             for(var i=0;i<$scope.days.length;i++){
	              for (var j = $scope.days[i].answers.length - 1; j >= 0; j--){
	                if($scope.days[i].answers.length > 1){
	                  if($scope.days[i].answers[j].category==null){
	                    $scope.days[i].answers.splice(j,1);
	                    //if(j < ($scope.days[i].answers.length-1)){
	                      for(var k=j;k<$scope.days[i].answers.length;k++){
	                      	//console.log('raw data',i,k,$scope.listOfSubCategory[i][k+1]);
	                        $scope.listOfSubCategory[i][k]=$scope.listOfSubCategory[i][k+1];
	                      }
	                   // }
	                  }
	                }else{
	                  if($scope.days[i].answers[j].category==null){
	                    $scope.days[i].answers[j].category=null;
	                    $scope.days[i].answers[j].subcategory=null;
	                    $scope.days[i].answers[j].comment='';
	                  }else{

	                  }
	                }
	              }
	             }
	          },1000)
	        },function errorCallback(response) {
	        })
        }

    }
	$scope.editProductViewData=function(memberid,currentPage){
		$scope.showForm=true;
		var pdata= sessionStorage.getItem('page');
		if(pdata !=''){
			sessionStorage.removeItem('page');
			sessionStorage.setItem('page',currentPage);
		}else{
			sessionStorage.setItem('page',currentPage);
		}
		//$scope.answers={};
		id=memberid;
		$scope.days=[];
		$http({
			method:'GET',
			url:"php/customerInfo.php?action=day",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				obj.answers = [];
				obj.chkbox=[];
                $scope.addNewRow(obj.answers);
			    $scope.days.push(obj);
				$scope.selectedAnswers.push(obj);
			})
			//console.log('edit clear',$scope.selectedAnswers);
			//console.log('days',$scope.days);
		},function errorCallback(response) {
		})
		$scope.example14model = [];
	    $scope.example14settings = {
	        scrollableHeight: '200px',
	        scrollable: true,
	        enableSearch: true
	    };
	    $scope.listOfSpecial=[];
		$scope.example14data = [];
		$http({
			method:'GET',
			url:"php/customerInfo.php?action=special",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.special_name,'value':obj.special_id};
				var data1={'label':obj.special_name,'id':obj.special_id};
				$scope.listOfSpecial.push(data);
				$scope.example14data.push(data1);
			})
		},function errorCallback(response) {
		})
		$scope.example2settings = {
	        displayProp: 'id'
	    };
		
		$scope.listOfCategory=[];
		$scope.listOfSubCategory =[]; 
	/*	$http({
			method:'GET',
			url:"php/customerInfo.php?action=cat",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.cat_name,'value':obj.cat_id};
				$scope.listOfCategory.push(data);
			})
			console.log('cata',$scope.listOfCategory);
		},function errorCallback(response) {
		})*/
		
		var memid=$.param({'action':'infoedit','member_id':id});
		$scope.mulImage=[];
        $scope.mulImage.push({'image':null});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:memid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('edit res',response.data);
			$scope.name=response.data[0].rest_name;
			$scope.getQuadrantName(response.data[0].quadrant);
			//$scope.quadrant.value=response.data[0].quadrant;
			//$scope.special.value=response.data[0].special;
          // $scope.timestamp = $filter('date')(new Date(response.data[0].date.replace("-","/")),'dd-MM-yyyy HH:mm:ss a');
            $scope.timestamp =response.data[0].date;
			$scope.citycode.value=response.data[0].city;
			$scope.proviance=response.data[0].proviance;
			$scope.postal=response.data[0].postal;
			$scope.address=response.data[0].address;
           // $scope.special.value=response.data[0].special;
            var specArr=response.data[0].special.split(",");
            var stadata=$.param({'action':'chkStatusSpecial','spec_ids':response.data[0].special});
            $http({
            	method:'POST',
            	url:'php/customerInfo.php',
            	data:stadata,
            	headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            }).then(function successCallback(response){
            	console.log("res obj",response.data);
            	angular.forEach(response.data,function(obj){
	            	var data={'id':obj.special_id};
	            	$scope.example14model.push(data);
	            })
            },function errorCallback(response) {
            })
            $timeout(function(){
            	$scope.setCategoryValue();
            },1000)
			$scope.country=response.data[0].country;
			$scope.latitude=response.data[0].latitude;
			$scope.longitude=response.data[0].longitude;
			$scope.businessno=response.data[0].business_phone_no;
			$scope.conperson=response.data[0].person;
			$scope.mobno=response.data[0].mobile;
			$scope.email=response.data[0].email;
			$scope.url=response.data[0].url;
			$scope.status=response.data[0].status;
			$scope.premium_service=response.data[0].premium;
            $scope.parentrestname={};
            if(response.data[0].parent_member_id != 'null'){
                $scope.parentrestname.value=response.data[0].parent_member_id;
            }
			if(response.data[0].image !=''){
				$scope.attchImage1="upload/"+response.data[0].image;
				$scope.imageData=response.data[0].image;
			}
			if(response.data[0].image ==''){
				$scope.attchImage1="img/no_image.jpg";
				$scope.imageData='';
			}
			var multiImage=response.data[0].multiple_image;
			var array=multiImage.split(",");
			console.log("arr image",$scope.mulImage);
			$scope.attach=[];
			$scope.dis=[];
			for(var i=0;i<array.length;i++){
				if(i==0){
					$scope.mulImage[i]['filename']=array[i];
				}
				if(i !=0){
				 $scope.mulImage.push({'image':null,'filename':array[i]});
				}
				 
			}
			var memberid=$.param({'action':'detailedit','member_id':id});
			$http({
				method:'POST',
				url:"php/customerInfo.php",
				data:memberid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				//console.log('resdetails',response.data=='null');
			$timeout(function(){
				if(response.data !='null' && response.data !=''){
				var disparr=[];
				var counter = {};
				
                response.data.map(function(item) {
                   counter[item.day_id] = (counter[item.day_id] || 0) + 1;
                });
				console.log('counter',counter);
				var mondayarr=[];
				var tuesdayarr=[];
				var wedensdayarr=[];
				var thrusdayarr=[];
				var fridayarr=[];
				var saturdayarr=[];
				var sundayarr=[];
				
				for(var i=0;i<response.data.length;i++){
					if(response.data[i].day_id==1){
						mondayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==2){
						tuesdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==3){
						wedensdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==4){
						thrusdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==5){
						fridayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==6){
						saturdayarr.push(response.data[i]);
					}
					if(response.data[i].day_id==7){
						sundayarr.push(response.data[i]);
					}
				}
				console.log('day arr',thrusdayarr=='');
				if(mondayarr !=''){
				  $scope.days[0].answers=[];
				}
				if(tuesdayarr !=''){
				  $scope.days[1].answers=[];
				}
				if(wedensdayarr !=''){
				  $scope.days[2].answers=[];
				}
				if(thrusdayarr !=''){
				 $scope.days[3].answers=[];
				}
				if(fridayarr !=''){
				  $scope.days[4].answers=[];
				}
				if(saturdayarr !=''){
				  $scope.days[5].answers=[];
				}
				if(sundayarr !=''){
				 $scope.days[6].answers=[];
				}
				for(var i=0;i<mondayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(mondayarr[i].checked==1){
						$scope.days[0].answers.push({
								category:{'value':mondayarr[i].cat_id},
								subcategory: null,
								comment: mondayarr[i].comment,
						})
						//console.log('checkbox',$scope.days[0].answers);
						$scope.setSubcatag(0,i,mondayarr[i].cat_id,mondayarr[i].subcat_id,1);
					}else{
						//console.log('listOf',$scope.listOfCategory);
						$scope.days[0].answers.push({
								category:{'value':mondayarr[i].cat_id},
								subcategory: null,
								comment: mondayarr[i].comment
						})
						//console.log('checkbox',$scope.days[0].answers[i].category);
						$scope.setSubcatag(0,i,mondayarr[i].cat_id,mondayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<tuesdayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(tuesdayarr[i].checked==1){
						$scope.days[1].answers.push({
								category:{'value':tuesdayarr[i].cat_id},
								subcategory: null,
								comment: tuesdayarr[i].comment,
						})
						$scope.setSubcatag(1,i,tuesdayarr[i].cat_id,tuesdayarr[i].subcat_id,1);
					}else{
						$scope.days[1].answers.push({
								category:{'value':tuesdayarr[i].cat_id},
								subcategory: null,
								comment: tuesdayarr[i].comment
						})
						$scope.setSubcatag(1,i,tuesdayarr[i].cat_id,tuesdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<wedensdayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(wedensdayarr[i].checked==1){
						$scope.days[2].answers.push({
								category:{'value':wedensdayarr[i].cat_id},
								subcategory: null,
								comment: wedensdayarr[i].comment,
						})
						$scope.setSubcatag(2,i,wedensdayarr[i].cat_id,wedensdayarr[i].subcat_id,1);
					}else{
						$scope.days[2].answers.push({
								category:{'value':wedensdayarr[i].cat_id},
								subcategory: null,
								comment: wedensdayarr[i].comment
						})
						$scope.setSubcatag(2,i,wedensdayarr[i].cat_id,wedensdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<thrusdayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(thrusdayarr[i].checked==1){
						$scope.days[3].answers.push({
								category:{'value':thrusdayarr[i].cat_id},
								subcategory: null,
								comment: thrusdayarr[i].comment,
						})
						$scope.setSubcatag(3,i,thrusdayarr[i].cat_id,thrusdayarr[i].subcat_id,1);
					}else{
						$scope.days[3].answers.push({
								category:{'value':thrusdayarr[i].cat_id},
								subcategory: null,
								comment: thrusdayarr[i].comment,
						})
						$scope.setSubcatag(3,i,thrusdayarr[i].cat_id,thrusdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<fridayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(fridayarr[i].checked==1){
						$scope.days[4].answers.push({
								category:{'value':fridayarr[i].cat_id},
								subcategory: null,
								comment: fridayarr[i].comment,
						})
						$scope.setSubcatag(4,i,fridayarr[i].cat_id,fridayarr[i].subcat_id,1);
					}else{
						$scope.days[4].answers.push({
								category:{'value':fridayarr[i].cat_id},
								subcategory: null,
								comment: fridayarr[i].comment,
						})
						$scope.setSubcatag(4,i,fridayarr[i].cat_id,fridayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<saturdayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(saturdayarr[i].checked==1){
						$scope.days[5].answers.push({
								category:{'value':saturdayarr[i].cat_id},
								subcategory: null,
								comment: saturdayarr[i].comment,
						})
						$scope.setSubcatag(5,i,saturdayarr[i].cat_id,saturdayarr[i].subcat_id,1);
					}else{
						$scope.days[5].answers.push({
								category:{'value':saturdayarr[i].cat_id},
								subcategory: null,
								comment: saturdayarr[i].comment
						})
						$scope.setSubcatag(5,i,saturdayarr[i].cat_id,saturdayarr[i].subcat_id,0);
					}
				}
				for(var i=0;i<sundayarr.length;i++){
					
					$scope.listOfSubCategory =[]; 
					if(sundayarr[i].checked==1){
						$scope.days[6].answers.push({
								category:{'value':sundayarr[i].cat_id},
								subcategory: null,
								comment: sundayarr[i].comment,
						})
						$scope.setSubcatag(6,i,sundayarr[i].cat_id,sundayarr[i].subcat_id,1);
					}else{
						$scope.days[6].answers.push({
								category:{'value':sundayarr[i].cat_id},
								subcategory: null,
								comment: sundayarr[i].comment
						})
						$scope.setSubcatag(6,i,sundayarr[i].cat_id,sundayarr[i].subcat_id,0);
					}
				}
				console.log('days',$scope.days);
				}
			},1000);
			},function errorCallback(response) {
			})
		},function errorCallback(response) {
		})
		$scope.buttonName="Update";
		$scope.ClearbuttonName="Cancel";
		$scope.showCancel=true;
	}
	$scope.clearCustomerInfoData=function(){
		//$state.go('dashboard.customer.view',{}, { reload: true });
		$scope.showForm=false;
		//$scope.selectedAnswers = [];
		var data=sessionStorage.getItem('page');
		$scope.currentPage=data;
		if($scope.selectedAnswers.length > 0){
			for(var i=0;i<$scope.selectedAnswers.length;i++){
				$scope.selectedAnswers[i].chkbox=[]
			}
		}
		$scope.mulImage=[];
        $scope.mulImage.push({'image':null});
		//console.log('clear',$scope.selectedAnswers);
	}
	$scope.uploadFile = function(event){
		//console.log('event',event.target.files);
        var files = event.target.files;
		for (var i = 0; i < files.length; i++) {
             var file = files[i];
             var reader = new FileReader();
             reader.onload = $scope.imageIsLoaded; 
             reader.readAsDataURL(file);
     }
    };
	$scope.imageIsLoaded = function(e){
    $scope.$apply(function() {
        $scope.attchImage1=e.target.result;
    });
	}
	$scope.onFileSelect = function($files) {
		 fileURL=$files;
		 $scope.imageData='';
	}
	var arr=[];
	$scope.updateResturantDetails=function(billdata){
		console.log('haii',billdata.$valid);
		//console.log('answers',$scope.days);
		if(billdata.$valid){
			if($scope.name==null || $scope.name==''){
				alert('Please add Business Name');
				focusInputField.borderColor('colgname1');
			}/*else if($scope.quadrant.value==null || $scope.quadrant.value==''){
				alert('Please select quadrant value');
				focusInputField.borderColor('quadrant');
			}else if($scope.special.value==null || $scope.special.value==''){
				alert('Please select the special');
				focusInputField.borderColor('special');
			}*/else if($scope.address==null || $scope.address==''){
				alert('Please add address');
				focusInputField.borderColor('address');
			}else if($scope.example14model.length ==0){
				alert('Please select special');
				focusInputField.borderColor('specials');
			}else if($scope.citycode.value==null || $scope.citycode.value==''){
				alert('Please select city');
				focusInputField.borderColor('citycode');
			}else if($scope.proviance==null || $scope.proviance==''){
				alert('Please add Province');
				focusInputField.borderColor('proviance');
			}else if($scope.postal==null || $scope.postal==''){
				alert('Please add postal code');
				focusInputField.borderColor('postal');
			}else if($scope.country==null || $scope.country==''){
				alert('Please add the country');
				focusInputField.borderColor('con');
			}else if($scope.latitude==null || $scope.latitude==''){
				alert('please add the latitude');
				focusInputField.borderColor('latitude');
			}else if(isNaN(document.getElementById("latitude").value)){
                alert('Letters are not allowed for latitude.Use only number');
            }else if($scope.longitude==null || $scope.longitude==''){
				alert('Please add the longitude');
				focusInputField.borderColor('longitude');
			}else if(isNaN(document.getElementById("longitude").value)){
                alert('Letters are not allowed for Longitude.Use only number');
            }else if($scope.businessno==null || $scope.businessno==''){
				alert('Please add business no');
				focusInputField.borderColor('businessno');
			}else if($scope.conperson==null || $scope.conperson==''){
				alert('Please add contact person');
				focusInputField.borderColor('conper');
			}else if($scope.mobno==null || $scope.mobno==''){
				alert('Please add contact number');
				focusInputField.borderColor('mobno');
			}else if($scope.email==null || $scope.email==''){
				alert('Please add the email');
				focusInputField.borderColor('emailid');
			}else if($scope.status==null || $scope.status==''){
				alert('Please select status');
				focusInputField.borderColor('status');
			}else if($scope.premium_service==null || $scope.premium_service==''){
				alert('Please select premium service');
				focusInputField.borderColor('service');
			}/*else if($scope.file==null && $scope.status==1 && $scope.imageData==''){
				alert('Please select the image');
				focusInputField.borderColor('imagefile1');
			}*/else if($scope.showpass==false && ($scope.password==null || $scope.password=='')){
				alert('Please add password');
				focusInputField.borderColor('passno');
			}else{
				console.log('hello',$scope.imageData!='',$scope.imageData=='',$scope.latitude);
				var supplierData='';
                var parentBusiness='';
                if($scope.parentrestname==undefined){
                    parentBusiness='';
                }else{
                    parentBusiness=$scope.parentrestname.value;
                }
				if($scope.imageData!=''){
					var mulImageString='';
					var arrImage=[];
					if($scope.mulImage.length>0){
						for(var i=0;i<$scope.mulImage.length;i++){
							if($scope.mulImage[i]['image']!=null){
								var newmulpath='';
								var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
								//$scope.mulImage[i]['image'].name=newmulpath;
								$scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
								arrImage.push({'image':$scope.mulImage[i]['image']});
								if(i==0){
									mulImageString=newmulpath;
								}else{
									mulImageString +=","+newmulpath;
								}
							}else{
								var newmulpath='';
								newmulpath=$scope.mulImage[i]['filename'];
								if(i==0){
									mulImageString=newmulpath;
								}else{
									mulImageString +=","+newmulpath;
								}
							}
						}
						if(arrImage.length>0){
							console.log('arr iamge',arrImage,$scope.mulImage);
							$scope.upload=Upload.upload({
								 url: 'php/uploadAll.php',
								 method: 'POST',
								 file: arrImage
							}).success(function(data, status, headers, config) {
								supplierData=$("#billdata").serialize();
								supplierData+= "&image="+$scope.imageData+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
								$http({
								method:'POST',
								url:"php/customerInfo.php",
								data:supplierData,
								headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
							}).then(function successCallback(response){
								console.log('res first',response.data);
								if(response.data['data']==1){
									
									for(var i=0;i<$scope.days.length;i++){
										for(var j=0;j<$scope.days[i].answers.length;j++){
											if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
												if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
												 arr.push(data);
											}
										}
									}
									console.log('arr',arr)
									
									var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
									//console.log('detail',detailddata);
									$http({
										method:'POST',
										url:"php/customerInfo.php",
										data:detailddata,
										headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
										console.log('res',response.data);
										alert(response.data['msg']);
										$state.go('dashboard.customer.view',{}, { reload: true });
									},function errorCallback(response) {
									})
								}
							},function errorCallback(response) {
								console.log('err',response.data);
								alert(response.data['msg']);
							})
							}).error(function(data,status){
							})
						}else{
							//console.log('arr 1',arrImage,arrImage.length);
							supplierData=$("#billdata").serialize();
								supplierData+= "&image="+$scope.imageData+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
								// console.log('supplier data1',supplierData);
								$http({
								method:'POST',
								url:"php/customerInfo.php",
								data:supplierData,
								headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
							}).then(function successCallback(response){
								console.log('res first',response.data);
								if(response.data['data']==1){
									
									for(var i=0;i<$scope.days.length;i++){
										for(var j=0;j<$scope.days[i].answers.length;j++){
											if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
												if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
												 arr.push(data);
											}
										}
									}
									
									console.log('arr',arr)
									var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
									//console.log('detail',detailddata);
									$http({
										method:'POST',
										url:"php/customerInfo.php",
										data:detailddata,
										headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
										console.log('res',response.data);
										alert(response.data['msg']);
										$state.go('dashboard.customer.view',{}, { reload: true });
									},function errorCallback(response) {
									})
								}
							},function errorCallback(response) {
								console.log('err',response.data);
								alert(response.data['msg']);
							})
						}
							
					}
					
				}
				if($scope.imageData==''){
					if($scope.file==null){
						var newpath='';
						var mulImageString='';
						var arrImage=[];
						if($scope.mulImage.length>0){
							for(var i=0;i<$scope.mulImage.length;i++){
								if($scope.mulImage[i]['image']!=null){
									var newmulpath='';
								    var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								    newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
								   // $scope.mulImage[i]['image'].name=newmulpath;
								   $scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
								    arrImage.push({'image':$scope.mulImage[i]['image']});
									if(i==0){
										mulImageString=newmulpath;
									}else{
										mulImageString +=","+newmulpath;
									}
								}else{
									var newmulpath='';
								    newmulpath=$scope.mulImage[i]['filename'];
									if(i==0){
										mulImageString=newmulpath;
									}else{
										mulImageString +=","+newmulpath;
									}
								}
							}
							if(arrImage.length>0){
								$scope.upload=Upload.upload({
									url: 'php/uploadAll.php',
									method: 'POST',
									file: arrImage
								}).success(function(data, status, headers, config) {
									supplierData=$("#billdata").serialize();
									supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                    //console.log('supplier',supplierData);
									$http({
										method:'POST',
										url:"php/customerInfo.php",
										data:supplierData,
										headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
										console.log('res 2nd',response.data);
										if(response.data['data']==1){
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
													 arr.push(data);
												}
											}
										}
										
										var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
										
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.view',{}, { reload: true });
										},function errorCallback(response) {
										})
									}
									},function errorCallback(response) {
										alert(response.data['msg']);
									})
								}).error(function(data,status){
								})
							}else{
								supplierData=$("#billdata").serialize();
								supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
                                 console.log('supplier',supplierData);
								$http({
										method:'POST',
										url:"php/customerInfo.php",
										data:supplierData,
										headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
									}).then(function successCallback(response){
										console.log('res 2nd',response.data);
										if(response.data['data']==1){
										for(var i=0;i<$scope.days.length;i++){
											for(var j=0;j<$scope.days[i].answers.length;j++){
												if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
													if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
													 arr.push(data);
												}
											}
										}
										
										var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
										
										$http({
											method:'POST',
											url:"php/customerInfo.php",
											data:detailddata,
											headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
										}).then(function successCallback(response){
											console.log('res',response.data);
											alert(response.data['msg']);
											$state.go('dashboard.customer.view',{}, { reload: true });
										},function errorCallback(response) {
										})
									}
									},function errorCallback(response) {
                                        console.log("error",response.data);
										alert(response.data['msg']);
									})
							}
						}
						
					}else{
						var mulImageString='';
						var arrImage=[];
						var file=fileURL;
						var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
						var newpath=today+"_"+ file.name;
						//file.name=newpath;
						file = Upload.rename(file, newpath);
						$scope.upload = Upload.upload({
							url: 'php/upload.php',
							method: 'POST',
							file: file
						}).success(function(data, status, headers, config) {
							if($scope.mulImage.length>0){
								for(var i=0;i<$scope.mulImage.length;i++){
									if($scope.mulImage[i]['image']!=null){
										var newmulpath='';
								        var today=(Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
								        newmulpath=today+"_"+ $scope.mulImage[i]['image'].name;
								       // $scope.mulImage[i]['image'].name=newmulpath;
									   $scope.mulImage[i]['image']=Upload.rename($scope.mulImage[i]['image'], newmulpath);
								        arrImage.push({'image':$scope.mulImage[i]['image']});
										if(i==0){
											mulImageString=newmulpath;
										}else{
											mulImageString +=","+newmulpath;
										}
									}else{
										var newmulpath='';
								        newmulpath=$scope.mulImage[i]['filename'];
										if(i==0){
											mulImageString=newmulpath;
										}else{
											mulImageString +=","+newmulpath;
										}
									}
								}
								if(arrImage.length>0){
									$scope.upload=Upload.upload({
										url: 'php/uploadAll.php',
										method: 'POST',
										file: arrImage
									}).success(function(data, status, headers, config) {
										supplierData=$("#billdata").serialize();
										supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
										$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:supplierData,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												console.log('res 2nd',response.data);
												if(response.data['data']==1){
													
													
													for(var i=0;i<$scope.days.length;i++){
														for(var j=0;j<$scope.days[i].answers.length;j++){
															if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
																if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
																 arr.push(data);
															}
														}
													}
													
													var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
													$http({
														method:'POST',
														url:"php/customerInfo.php",
														data:detailddata,
														headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
													}).then(function successCallback(response){
														console.log('res',response.data);
														alert(response.data['msg']);
														$state.go('dashboard.customer.view',{}, { reload: true });
													},function errorCallback(response) {
													})
												}
											},function errorCallback(response) {
												alert(response.data['msg']);
											})
									}).error(function(data,status){
									})
								}else{
									supplierData=$("#billdata").serialize();
									supplierData+= "&image="+newpath+"&action=update"+"&quadrant="+$scope.quadrant.value+"&mulimage="+mulImageString+"&member_id="+id+"&latitude="+$scope.latitude+"&cityid="+$scope.citycode.value+"&longitude="+$scope.longitude+"&special="+JSON.stringify($scope.example14model)+"&parentbusiness="+parentBusiness;
									$http({
												method:'POST',
												url:"php/customerInfo.php",
												data:supplierData,
												headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
											}).then(function successCallback(response){
												console.log('res 2nd',response.data);
												if(response.data['data']==1){
													
													
													for(var i=0;i<$scope.days.length;i++){
														for(var j=0;j<$scope.days[i].answers.length;j++){
															if($scope.days[i].answers[j].category || $scope.days[i].answers[j].subcategory || $scope.days[i].answers[j].comment){
																if($scope.days[i].answers[j].hasOwnProperty("check") && $scope.days[i].answers[j].check){
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':1};
												}else{
													var data={'day':$scope.days[i].day_id,'cata':$scope.days[i].answers[j].category.value,'subcat':$scope.days[i].answers[j].subcategory.value,'comment':document.getElementById('answer_'+j+'_'+i+'_comment').value,'pos':i,'childindex':j,'check':0};
												}
																 arr.push(data);
															}
														}
													}
													
													var detailddata=$.param({'action':'subcatupdate','tableData':arr,'member_id':id,'quadrant':$scope.quadrant.value,'result':response.data['data'],'city':$scope.citycode.value});
													$http({
														method:'POST',
														url:"php/customerInfo.php",
														data:detailddata,
														headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
													}).then(function successCallback(response){
														console.log('res',response.data);
														alert(response.data['msg']);
														$state.go('dashboard.customer.view',{}, { reload: true });
													},function errorCallback(response) {
													})
												}
											},function errorCallback(response) {
												alert(response.data['msg']);
											})
								}
							}
							
						}).error(function(data, status) {
						})
					}
				}
				//end
			}
		}else{
			if(billdata.bannerimage.$invalid){
				alert('Please add the valid image(i.e-.png or .jpeg)  of size 2 mb max');
				focusInputField.borderColor('imagefile1');
			}
			if(billdata.url.$invalid){
				alert('Please add a valid URL with http:// or https://');
				focusInputField.borderColor('weburl');
			}
			if(billdata.businessno.$invalid){
				alert('Please add a valid phone number');
				focusInputField.borderColor('businessno');
			}
			if(billdata.mobno.$invalid){
				alert('Please add a valid phone number');
				focusInputField.borderColor('mobno');
			}
		}
	}
	$scope.clearField=function(id){
		focusInputField.clearBorderColor(id);
	}
	$scope.deleteProductInfoData=function(memberid){
		id=memberid;
		var delid=$.param({'action':'del','member_id':memberid});
		console.log('del',delid)
		var conmesg=$window.confirm('Are you sure to delete this list?');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/customerInfo.php",
				data:delid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res del',response.data);
				alert(response.data['msg']);
				$state.go('dashboard.customer.view',{}, { reload: true });
			},function errorCallback(response) {
			})
		}
	}
	$scope.hidebtn=true;
    $scope.showpass=true;
	$scope.resetPasswod=function(){
		$scope.hidebtn=false;
		$scope.showpass=false;
		$scope.rejectpass=true;
	}
	$scope.closePass=function(){
		$scope.showpass=true;
		$scope.rejectpass=false;
		$scope.hidebtn=true;
	}
	$scope.listOfSubCatagory1 = []; 
	$scope.setCatagory=function(index,value1,value2,name1,name2){
		if(value1==value2){
			alert(name1+" is already selected");
		}
		$scope.listOfSubCatagory1[index]=[];
		//$scope.dropdownValue[index]=[];
		var catdata=$.param({'action':'subcat','cat_id':value2});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:catdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.subcat_name,'value':obj.subcat_id};
				$scope.listOfSubCatagory1[index].push(data);
			})
		},function errorCallback(response) {
		})
	}
	$scope.listOfSubCatagory = []; 
	/*$scope.removeBorder=function(id,index,catvalue,catvalue1,name1,name2){
		if(catvalue1){
			if(catvalue==catvalue1){
				alert(name1+" is already selected");
			}
		}
		$scope.listOfSubCatagory[index]=[];
		//$scope.dropdownValue[index]=[];
		var catdata=$.param({'action':'subcat','cat_id':catvalue});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:catdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.subcat_name,'value':obj.subcat_id};
				$scope.listOfSubCatagory[index].push(data);
			})
		},function errorCallback(response) {
		})
	}*/
$scope.removeBorder=function(index,catvalue,parent,com){
	if(catvalue !=undefined){
     var subcategories=[];
	 var catdata=$.param({'action':'subcat','cat_id':catvalue});
		$http({
			method:'POST',
			url:"php/customerInfo.php",
			data:catdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.subcat_name,'value':obj.subcat_id};
				if(!$scope.listOfSubCategory[parent]){
                      $scope.listOfSubCategory[parent] = [];  
                }
				//console.log('data',data);
				subcategories.push(data);
			})
			$scope.listOfSubCategory[parent][index]=subcategories;
		},function errorCallback(response) {
		}) 
	}
  }
   $scope.onFileSelect1=function(index){
	   $scope.mulImage[index]['filename']='';
   }
   $scope.getSortingValue=function(result,cb){
	  // console.log('result',result);
	    var object = {},
            array = [];
        result.forEach(function (a) {
            var k = a.member_id ;
            if (!object[k]) {
                object[k] = {date: a.date,rest_name:a.rest_name, member_id: a.member_id, quadrant_name: a.quadrant_name, city:a.city,	proviance:a.proviance,postal:a.postal,country:a.country,contact_person:a.person,mobile:a.mobile,url:a.url,status:a.status,premium:a.premium,business_phone_no:a.business_phone_no,email:a.email, special:[] };
                array.push(object[k]);
            }
			if(a.day_name !=null && a.cat_name !=null && a.subcat_name !=null && a.comment !=null){
            object[k].special.push({ day: a.day_name, cat_name:a.cat_name,subcat_name:a.subcat_name,comment:a.comment});
			//object[k].order='order_status:'+ a.status+', order_id:'+ a.order_id+',ordered_quantity:'+a.ordered_quantity+',prod_tot_price:'+a.prod_tot_price+',delstatus:'+a.delstatus+',name:'+a.name+',date:'+a.order_date;
			}else{
				  object[k].special.push({ day: 'NA', cat_name: 'NA',subcat_name:'NA',comment:'NA'});
			}
			//onsole.log(object[k]);
        });
		cb(array);
   }
   /*$scope.generateExcelSheet=function(){
            var browser=browserDetectionforRest.detectBrowserRestType();
            console.log('browser',browser);
			var uri = 'data:application/vnd.ms-excel;base64,'
			, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
			, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			  var table='exportable';
			  var name='Report';
			 // console.log('table',table,name);
			if (!table.nodeType) table = document.getElementById(table)
			var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			window.location.href = uri + base64(format(template, ctx));
         //  var anchr=document.createElement('a');
          // anchr.href=uri + base64(format(template, ctx));
          // anchr.download = prompt('enter filename', 'new.xls');
          // anchr.click();
		  
   }
   $scope.generateExcelSheetForEdge=function(){
       var csv = tableToCSV(document.querySelector( "#sites" ));
       var blob = new Blob( [ csv ], { type: "text/html"} );
       if ( navigator.msSaveOrOpenBlob ) {
        // Works for Internet Explorer and Microsoft Edge
        navigator.msSaveOrOpenBlob( blob, "output.csv" );

      } else {

            // Attempt to use an alternative method
            var anchor = document.body.appendChild(
              document.createElement( "a" )
            );
            // If the [download] attribute is supported, try to use it
            if ( "download" in anchor ) {
                  if(browser=='Edge'){
                    anchor.download = "output.csv";
                    anchor.href = URL.createObjectURL( blob );
                    anchor.click();
                }

          }
      }

      function tableToCSV( table ) {
        // We'll be co-opting `slice` to create arrays
        var slice = Array.prototype.slice;

        return slice.call( table.rows ).map(function ( row ) {
          return slice.call( row.cells ).map(function ( cell ) {
            return '"t"'.replace( "t", cell.textContent );
          }).join( "," );
        }).join( "\r\n" );

      }
   }*/
  /* $scope.exportData=function(){
      
	   $scope.memberIds=[];
	   angular.forEach($scope.labelResults,function(obj){
		   var data={'member_id':obj.member_id};
		   $scope.memberIds.push(data);
	   })
	   var searchData=$.param({'action':'search','member_data':$scope.memberIds});
	   $http({
		   method:'POST',
		   url:"php/customerInfo.php",
		   data:searchData,
		   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	   }).then(function successCallback(response){
		   //console.log('report data',response.data);
         //  alert('before callback');
		   $scope.getSortingValue(response.data,function(result){
			  // console.log('result',result);
              // alert('after callback');
			   $scope.listOfReportData=result;
               console.log('list of data', $scope.listOfReportData);
               //alert('list');
               var browser=browserDetectionforRest.detectBrowserRestType();
             //  alert(browser);
			   if($scope.listOfReportData){
                   if(browser=='Edge'){
                       $timeout(function(){
                           $scope.generateExcelSheetForEdge(); 
                       },3000);
                   }else{
                       $timeout(function(){
                            console.log('inside callback');
                            $scope.generateExcelSheet();
                        },3000);
                   }
					
				}
			   
		   })
	   },function errorCallback(response) {
	   })
   }*/
    $scope.generateExcelSheetSafari=function(){
			var uri = 'data:application/vnd.ms-excel;base64,'
			, template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
			, base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
			, format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
			  var table='exportable';
			  var name='Report';
			  //console.log('table',table,name);
			if (!table.nodeType) table = document.getElementById(table)
            // console.log('table',table);
			var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
			window.location.href = uri + base64(format(template, ctx));
		  
   }
    $scope.generateExcelSheet=function(){
        var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "." + month + "." + year + "_" + hour + "." + mins;
        var table=document.getElementById('exportable1').innerHTML;
        var table1="<html></head><body><table>"+table+"</table></body></html>";
        var myBlob = new Blob([table1], {
          type: 'application/vnd.ms-excel'
        });
        var url = window.URL.createObjectURL(myBlob);
        var a = document.createElement("a");
        document.body.appendChild(a);
        a.href = url;
        a.download ='exported_businessinfo_' + postfix + '.xlsx'; 
        a.click();
        //adding some delay in removing the dynamically created link solved the problem in FireFox
        setTimeout(function() {
          window.URL.revokeObjectURL(url);
        }, 0);
        
    }
    $scope.generateExcelSheetForEdge=function(){
       var csv = tableToCSV(document.querySelector( "#exportable1" ));
       var blob = new Blob( [ csv ], { type: "text/html"} );
       if ( navigator.msSaveOrOpenBlob ) {
        // Works for Internet Explorer and Microsoft Edge
        navigator.msSaveOrOpenBlob( blob, "output.csv" );

      } else {

            // Attempt to use an alternative method
            var anchor = document.body.appendChild(
              document.createElement( "a" )
            );
            // If the [download] attribute is supported, try to use it
            if ( "download" in anchor ) {
                  if(browser=='Edge'){
                    anchor.download = "output.csv";
                    anchor.href = URL.createObjectURL( blob );
                    anchor.click();
                }

          }
      }

      function tableToCSV( table ) {
        // We'll be co-opting `slice` to create arrays
        var slice = Array.prototype.slice;

        return slice.call( table.rows ).map(function ( row ) {
          return slice.call( row.cells ).map(function ( cell ) {
            return '"t"'.replace( "t", cell.textContent );
          }).join( "," );
        }).join( "\r\n" );

      }
   }
    $scope.exportData=function(){
	   $scope.memberIds=[];
	   angular.forEach($scope.labelResults,function(obj){
		   var data={'member_id':obj.member_id};
		   $scope.memberIds.push(data);
	   })
	   var searchData=$.param({'action':'search','member_data':$scope.memberIds});
	   $http({
		   method:'POST',
		   url:"php/customerInfo.php",
		   data:searchData,
		   headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	   }).then(function successCallback(response){
		//   console.log('report data',response.data);
		   $scope.getSortingValue(response.data,function(result){
			 //console.log('result after sorting',result);
			   $scope.listOfReportData=result;
               var browser=browserDetectionforRest.detectBrowserRestType();
               if(browser=='Edge'){
                   $timeout(function(){
                           $scope.generateExcelSheetForEdge(); 
                   },3000);
               }else if(browser=='Safari'){
                    if($scope.listOfReportData){
                          $timeout(function(){
                               $scope.generateExcelSheetSafari();
                          },3000);
                    }
               }else{
                   if($scope.listOfReportData){
                        $timeout(function(){
                           // console.log('hello');
                            $scope.generateExcelSheet();
                        },3000);
                    }
               }
			   
		   })
	   },function errorCallback(response) {
	   })
   }
  $scope.answerIsSelected = function(dayIdx, answerIdx) {
     var thisAnswer = $scope.days[dayIdx].answers[answerIdx];
    // return $scope.selectedAnswers.indexOf(thisAnswer) > -1;
    return $scope.selectedAnswers[dayIdx].chkbox.indexOf(thisAnswer) > -1;
  };
  
  $scope.toggleAnswerSelected = function(dayIdx, answerIdx) {
    var thisAnswer = $scope.days[dayIdx].answers[answerIdx];
		if ($scope.answerIsSelected(dayIdx, answerIdx)) {
		 // $scope.selectedAnswers.splice($scope.selectedAnswers.indexOf(thisAnswer), 1);
		 $scope.selectedAnswers[dayIdx].chkbox.splice($scope.selectedAnswers.indexOf(thisAnswer), 1);
		}else{
		   $scope.selectedAnswers[dayIdx].chkbox.push(thisAnswer);
		}
  };
  
  $scope.isDisabled = function(dayIdx, answerIdx) {
    //return !$scope.answerIsSelected(dayIdx, answerIdx) && $scope.selectedAnswers.length === 2;
	return !$scope.answerIsSelected(dayIdx, answerIdx) &&$scope.selectedAnswers[dayIdx].chkbox.length === 2;
  };
  $scope.getLatitude=function(address,callback){
	   geocoder = new google.maps.Geocoder();
	   if (geocoder) {
		   geocoder.geocode({
			   'address': address
		   },function (results, status) {
			   if (status == google.maps.GeocoderStatus.OK) {
				    callback(results[0]);
			   }
		   })
	   }
  }
  $scope.getLatValue=function(){
       var cityName='';
	  if($scope.address==null || $scope.address==''){
		  alert('Please enter the address');
	  }else if($scope.citycode.value==null || $scope.citycode.value==''){
          alert('Please select the city');
      }else if($scope.postal==null || $scope.postal==''){
          alert('Please enter the postal code');
      }else{
		  var citydata=$.param({'action':'getcity','city':$scope.citycode.value});
          $http({
              method:'POST',
              url:'php/customerInfo.php',
              data:citydata,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).then(function successCallback(response){
              cityName=response.data[0].city_name;
          },function errorCallback(response) {
          })
           $timeout(function(){
              var address=$scope.address+','+cityName+','+$scope.postal;
              $scope.getLatitude(address,function(result){
                 // console.log('result',result.geometry.location.lat());
                  $timeout(function(){
                    $scope.latitude=result.geometry.location.lat();
                    $scope.longitude=result.geometry.location.lng();
                  },1000);
              });
           },1000);
	  }
  }
  $scope.getLongValue=function(){
       var cityName='';
	 if($scope.address==null || $scope.address==''){
		  alert('Please enter the address');
	  }else if($scope.citycode.value==null || $scope.citycode.value==''){
          alert('Please select the city');
      }else if($scope.postal==null || $scope.postal==''){
          alert('Please enter the postal code');
      }else{
		   var citydata=$.param({'action':'getcity','city':$scope.citycode.value});
          $http({
              method:'POST',
              url:'php/customerInfo.php',
              data:citydata,
              headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          }).then(function successCallback(response){
              cityName=response.data[0].city_name;
          },function errorCallback(response) {
          })
		   $timeout(function(){
              var address=$scope.address+','+cityName+','+$scope.postal;
              $scope.getLatitude(address,function(result){
                 // console.log('result',result.geometry.location.lat());
                  $timeout(function(){
                    $scope.latitude=result.geometry.location.lat();
                    $scope.longitude=result.geometry.location.lng();
                  },1000);
              });
           },1000);
	  } 
  }
  $scope.startsWith = function (actual, expected) {
	//console.log('hello',$scope.listOfCustomerData);
    var lowerStr =(actual + "").toLowerCase();
	//console.log('htt',lowerStr.indexOf(expected.toLowerCase()) === 0);
    return lowerStr.indexOf(expected.toLowerCase()) === 0;
  }
  $scope.getMap=function(){
       var base_url = "index.html#" 
       var url = $state.href('map', {latitude:$scope.latitude,longitude:$scope.longitude,'title':$scope.address});
       $window.open(base_url+url,'_blank'); 
  }
});
customerView.directive('customOnChange', function() {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      var onChangeHandler = scope.$eval(attrs.customOnChange);
      element.bind('change', onChangeHandler);
    }
  };
});
customerView.factory('focusInputField',function($timeout, $window) {
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});
/*customerView.filter('startsWithLetter', function () {
    return function (items, letter) {
		//console.log('items',items);
		if(items != undefined){
        var filtered = [];
        var letterMatch = new RegExp(letter, 'i');
        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            if (letterMatch.test(item.rest_name.substring(0, 1))) {
                filtered.push(item);
            }
        }
        return filtered;
		}
    };
});*/
/*customerView.filter('startsWithLetter', function () {
    return function (items, search) {
		//console.log('items',items);
		if(items != undefined){
			if (!search) {
			return items;
		  }
		  search = search.toLowerCase();
		  return items.filter(function(element) {
			return element.rest_name.toLowerCase().substring(0, search.length).indexOf(search) != -1;
		  });
		}
    };
});*/
customerView.filter('startsWithLetter', function () {
    return function (items, search) {
		//console.log('items',items);
		if(items != undefined){
			if (!search) {
			return items;
		  }
		  search = search.toLowerCase();
		  return items.filter(function(element) {
			return element.rest_name.toLowerCase().indexOf(search) != -1;
		  });
		}
    };
});
customerView.factory('browserDetectionforRest',function($window,$timeout){
	return{
		detectBrowserRestType:function(){
            var objbrowserName; 
			 if((navigator.userAgent.indexOf("Edge") != -1 ) || (!!document.documentMode == true )){ //IF IE > 10
              //alert('Edge'); 
              objbrowserName= 'Edge';
            }else if(navigator.userAgent.indexOf("MSIE") != -1 ){
                // alert('IE');
                  objbrowserName='IE';
            } else if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
               // alert('Opera');
                 objbrowserName='Opera';
            }else if(navigator.userAgent.indexOf("Chrome") != -1 ){
               // alert('Chrome');
                objbrowserName='Chrome';
            }else if(navigator.userAgent.indexOf("Safari") != -1){
               // alert('Safari');
                objbrowserName='Safari';
            }else if(navigator.userAgent.indexOf("Firefox") != -1 ) {
                 objbrowserName='Firefox';
            }else{
               //alert('unknown');
            }
            return objbrowserName;
		}
	}
})