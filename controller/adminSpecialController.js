var special=angular.module('Spesh');
special.controller('adminSpecialController',function($scope,$http,$state,$window){
	var id='';
	$scope.submitbuttonName='Add';
	$scope.submitButton=true;
	$http({
		method:'GET',
		url:'php/category/special.php?action=disp',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objSpecialData=response.data;
	},function errorCallback(response) {
	})
	$scope.addSpecial=function(billdata){
		if(billdata.$valid){
			if($scope.specialname==null || $scope.specialname==''){
				alert('Please add the special name');
			}else if($scope.type==null || $scope.type==''){
				alert('Please add the order(The order  should be always numeric value.i.e-1,2,3...9)');
			}else{
				if($scope.submitbuttonName=='Add'){
					var addData=$.param({'action':'add','specialname':$scope.specialname,'type':$scope.type});
					$http({
						method:'POST',
						url:"php/category/special.php",
						data:addData,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						alert(response.data['msg']);
						$state.go('dashboard.category.special',{},{reload:true});
					},function errorCallback(response) {
						alert(response.data['msg']);
					})
				}
				if($scope.submitbuttonName=='Update'){
					var addData=$.param({'action':'update','specialname':$scope.specialname,'type':$scope.type,'special_id':id});
					$http({
					method:'POST',
					url:"php/category/special.php",
					data:addData,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						alert(response.data['msg']);
						$state.go('dashboard.category.special',{},{reload:true});
					},function errorCallback(response) {
						alert(response.data['msg']);
					})
				}
			}
		}else{
            if(billdata.type.$invalid){
                alert('This field needs only number(e.g-0,1..9)');
            }
        }
	}
	$scope.cancelUpdate =function(){
		$state.go('dashboard.category.special',{},{reload:true});
	}
	$scope.editSpecialData=function(cityid){
		id=cityid;
		var editid=$.param({'action':'edit','special_id':cityid});
		$http({
			method:'POST',
			url:"php/category/special.php",
			data:editid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.specialname=response.data[0].special_name;
			$scope.type=parseInt(response.data[0].type);
			$scope.cancelButton=true;
	 	    $scope.submitbuttonName="Update";
		},function errorCallback(response) {
		});
	}
	$scope.removeSpecialData=function(cityid){
		if(confirm("Are you sure want to delete special?")){
			var deletedata=$.param({'action':'delete','special_id':cityid});
			$http({
			method:'POST',
			url:"php/category/special.php",
			data:deletedata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.category.special',{},{reload:true});
			},function errorCallback(response) {
                alert(response.data['msg']);
				$state.go('dashboard.category.special',{},{reload:true});
			});
		}
	}
    $scope.inactiveSpecialData=function(special_id,status){
        var supplierData=$.param({'special_id':special_id,'action':'inactive','status':status});
       // console.log('action',supplierData);
        $http({
           method:'POST',
           url:'php/category/special.php',
           data:supplierData,
           headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function successCallback(response){
            console.log('action res',response.data);
           alert(response.data['msg']);
           $state.go('dashboard.category.special',{},{reload:true});
        },function errorCallback(response) {
           alert(response.data['msg']);
        })
    }
})
