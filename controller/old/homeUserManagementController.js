var dashboard=angular.module('Channabasavashwara');
dashboard.controller('homemanagementController',function($scope,$http,$state,$window){
	$scope.buttonName="Add";
	$scope.clearButtonName ="Cancel";
	$scope.showCancel = false;
	 var id='';
	 var log_name='';
	 var del_name='';
	 $scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }	
	 $http({
		method: 'GET',
		url: "php/homeUser/readUserData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objHomeUserData=response.data;
	},function errorCallback(response) {
		
	});
	 $scope.addHomeUserData=function(billdata){
		 //console.log('button name',$('#addProfileData')[0].defaultValue);
		 if(billdata.$valid){
		 if($('#addProfileData')[0].defaultValue=='Add'){
		 if($scope.first_name==null){
			 alert("First name field can not be blank");
		 }else if($scope.last_name==null){
			  alert("Last name field can not be blank");
		 }else if($scope.user_email==null){
			 alert("Email field can not be blank");
		 }else if($scope.mob_no==null){
			 alert("Mobile no  field can not be blank");
		 }else if($scope.login_name==null){
			 alert("Login Name  field can not be blank");
		 }else if($scope.password==null){
			 alert("password field can not be blank");
		 }else if($scope.user_status==null){
			  alert("select user status");
		 }else{
			 var userdata={'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status};
			 //console.log('user',userdata);
			 $http({
				 method: 'POST',
				 url: "php/homeUser/addUserData.php",
				 data: userdata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				// console.log(response);
				 alert(response.data['msg']);
			     $scope.first_name=null;
				 $scope.last_name=null;
			     $scope.user_email=null;
			     $scope.mob_no=null;
		         $scope.login_name=null;
				 $scope.password=null;
				 $scope.user_status=null;
			     $scope.objHomeUserData.unshift(response.data);
			 },function errorCallback(response) {
				 alert(response.data['msg']);
				 $state.go('home.usermanagement',{}, { reload: true });
			 });
		 }
		 }
		 if($('#addProfileData')[0].defaultValue=='Update'){
			 if($scope.first_name==null){
			 alert("first name field can not be blank");
		 }else if($scope.last_name==null){
			  alert("Last name field can not be blank");
		 }else if($scope.user_email==null){
			 alert("Email field can not be blank");
		 }else if($scope.mob_no==null){
			 alert("Mobile no  field can not be blank");
		 }else if($scope.login_name==null){
			 alert("Login Name  field can not be blank");
		 }else if($scope.user_status==null){
			  alert("select user status");
		 }else{
			 var updatedata={'first_name':$scope.first_name,'last_name':$scope.last_name,'user_email':$scope.user_email,'mob_no':$scope.mob_no,'login_name':$scope.login_name,'password':$scope.password,'user_status':$scope.user_status,'user_id':id,'log_name':log_name};
			 console.log('user',updatedata);
			 $http({
				 method: 'POST',
				 url: "php/homeUser/updateUserData.php",
				 data: updatedata,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			 }).then(function successCallback(response){
				 //console.log('updated',response);
				 alert(response.data['msg']);
			     $scope.first_name=null;
				 $scope.last_name=null;
			     $scope.user_email=null;
			     $scope.mob_no=null;
		         $scope.login_name=null;
				 $scope.password=null;
				 $scope.user_status=null;
			     $state.go('home.usermanagement',{}, { reload: true });
			 },function errorCallback(response) {
				// console.log('duplicate',response);
				 alert(response.data['msg']);
			 });
		 }
		 }
		  }else{
			  alert("This form is not valid");
		  }
	 }
	 $scope.editHomeUserData=function(uid){
		 id=uid;
		 $scope.showCancel = true;
		 var userdata={'userid':id};
		 $http({
			method: 'POST',
			url: "php/homeUser/editUserData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			$scope.first_name=response.data[0].first_name;
			$scope.last_name=response.data[0].last_name;
			$scope.mob_no=response.data[0].mob_no;
			$scope.user_email=response.data[0].email;
			$scope.login_name=response.data[0].login_name;
			log_name=response.data[0].login_name;
			$scope.user_status=response.data[0].user_status;
			$scope.buttonName="Update";
		},function errorCallback(response) {
		});
	 }
	 

	 
	  $scope.clearSubjectData=function(){
		   $state.go('home.usermanagement',{}, { reload: true });
	 }
	 
	 
	 $scope.deleteHomeUserData=function(uid,lname){
		 var deleteUser=$window.confirm('Are you absolutely sure you want to delete?');
		 if(deleteUser){
			 id=uid;
			 del_name=lname
			 var userdata={'userid':id,'login_name':del_name};
			 console.log('deleted data',userdata);
			 $http({
			method: 'POST',
			url: "php/homeUser/deleteUserData.php",
			data:userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('delete',response);
			alert(response.data);
			$state.go('home.usermanagement',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data);
			$state.go('home.usermanagement',{}, { reload: true });
		});
		 }
	 }
})