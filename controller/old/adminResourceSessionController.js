var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceSessionController',function($scope,$http,$state,$window,sessionField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readSessionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.sessionData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				sessionField.borderColor('colg_name');
			}else if($scope.session==null || $scope.session==''){
				alert('Please add Academic Year...');
				sessionField.borderColor('resourcesession');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'session':$scope.session};
				$http({
					method:'POST',
					url:"php/resource/addSessionData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					//$scope.colg_name.value='';
					$scope.session=null;
					//$scope.sessionData.unshift(response.data);
					//$state.go('dashboard.res.session',{}, { reload: true });
					if($scope.sessionData=='null'){
						$scope.sessionData=[];
						$scope.sessionData.push(response.data);
					}else{
						$scope.sessionData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.session==response.data.session){
						sessionField.borderColor('resourcesession');
					}else{
					//$state.go('dashboard.res.session',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				sessionField.borderColor('colg_name');
			}else if($scope.session==null || $scope.session==''){
				alert('Please add Academic Year...');
				sessionField.borderColor('resourcesession');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'session':$scope.session,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateSessionData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.session',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.session==response.data.session){
						sessionField.borderColor('resourcesession');
					}else{
					//$state.go('dashboard.res.session',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editSessionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.session=response.data[0].session;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Academic Year');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteSessionData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.session',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('dashboard.res.session',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			sessionField.clearBorderColor(id);
		}
	}

	$scope.clearField=function(id){
		sessionField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('dashboard.res.session',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('sessionField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});