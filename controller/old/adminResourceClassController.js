var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceClassController',function($scope,$http,$state,$window,classField){
	$scope.buttonName="Add";
	$scope.classData=[];
	var id='';
	$scope.listOfCollege=[{
		name:'Select College',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readClassData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.classData=response.data;
	},function errorCallback(response) {
	});
	$scope.addClassData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college name');
				classField.borderColor('colg_name');
			}else if($scope.subject==null || $scope.subject==''){
				alert('Subject Type field could not be blank...');
				classField.borderColor('resourcesub');
			}else if($scope.period=='' || $scope.period==null){
				alert('Please select the no of classes...');
				classField.borderColor('resourceperiod');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'sub_type':$scope.subject,'period':$scope.period};
				$http({
					method:'POST',
					url:"php/resource/addClassData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.subject=null;
					$scope.period=''
					if($scope.classData=='null'){
						$scope.classData=[];
						$scope.classData.push(response.data);
					}else{
						$scope.classData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.subject==response.data.type){
						classField.borderColor('resourcesub');
					}else{
					$state.go('dashboard.res.class',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select your college name');
				classField.borderColor('colg_name');
			}else if($scope.subject==null || $scope.subject==''){
				alert('Subject type could not be blank...');
				classField.borderColor('resourcesub');
			}else if($scope.period=='' || $scope.period==null){
				alert('Please select the no of classes...');
				classField.borderColor('resourceperiod');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'sub_type':$scope.subject,'id':id,'period':$scope.period};
				$http({
					method:'POST',
					url:"php/resource/UpdateClassData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.class',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.subject==response.data.type){
						classField.borderColor('resourcesub');
					}else{
					$state.go('dashboard.res.class',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editClassData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].colg_id;
			$scope.subject=response.data[0].type;
			$scope.period=response.data[0].period;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.periodread=true;
			$scope.colgdis=true;
			$scope.perioddis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSubjectData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Subject type');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteClassData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.class',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('dashboard.res.class',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			classField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		classField.clearBorderColor(id);
	}
	$scope.cancelClassData=function(){
		$state.go('dashboard.res.class',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('classField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});