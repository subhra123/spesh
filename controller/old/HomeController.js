var homeapp=angular.module('Channabasavashwara');
homeapp.controller('homeController',function($scope,$http,$state){
	$http({
		 method: 'GET',
		 url: 'php/Login/session.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.userTypeName=response.data[0].user_name;
		 $scope.getData(response.data[0].dept_id);
	 },function errorCallback(response) {
		 $state.go('/',{}, { reload: true });
	 });
	 $scope.logout=function(){
		 $http({
		 method: 'POST',
		 url: 'php/Login/logout.php',
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 console.log('session',response);
	     //alert(response);
	 },function errorCallback(response) {
		 //console.log('session',response);
		 //alert(response);
	 });
	 }
	 $scope.getData=function(usertype){
		 //console.log('user type',usertype);
		 userdata={'user_type':usertype};
		 $http({
		 method: 'POST',
		 url: 'php/Login/getDeptData.php',
		 data: userdata,
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	 }).then(function successCallback(response){
		 $scope.deptName=response.data[0].dept_name
		 
	 },function errorCallback(response) {
		 
	 });
	 }
})