var plan=angular.module('Channabasavashwara');
plan.controller('princpalwdsController',function($scope,$http,$state,$window,focusStreamField){
	
	
	$scope.user_readonly= false;
	$scope.showFirstForm=true;
	$scope.displayDepartment=true;
	$scope.displayFaculty=true;
	$scope.buttonName="Add";
	$scope.showAddUnit=false;
	
	$scope.unit_list=true;
	$scope.plan_list=false;
	
	$scope.topic = "";
	$scope.clearButtonName="Cancel";
	$scope.showCancel=false;
	var id='';
	$scope.listStatus=[];
	$scope.listStatus=[{
		name:'Not Completed',
		value:'0'} ,
		{
		name:'Completed',
		value:'1'}
	];
	
	$scope.status_name=$scope.listStatus[0];
	
	$scope.listFaculty=[];
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];
	
	$scope.listOfFillterdept=[{
		name:'select department',
		value:''
	}]
	$scope.deptName=$scope.listOfFillterdept[0];
	
	

	
	$scope.listPlanData=[];
	$scope.listPlanData=[{
		name:'Select Plan',
		value:''
	}
	];
	$scope.plan_name=$scope.listPlanData[0];
	
	
	$scope.listTimeData=[];
	$scope.listTimeData=[{
		name:'Select Time Slot',
		value:''
	}
	];
	
	$scope.time_slot=$scope.listTimeData[0];
	
	$http({
		method:'GET',
		url:"php/userwds/getTimeSlot.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.time_slot,'value':obj2.id};
			$scope.listTimeData.push(data);
		});
	},function errorCallback(response) {
	});
	
	
	
	$scope.listOfCourse=[{
		name:'Select Course',
		value:''
		}];
	$scope.course_name=$scope.listOfCourse[0];
	$scope.getListOfCourse=function(id){
		focusStreamField.removeBorderColor(id);
		$scope.listOfCourse=null;
		$scope.listOfCourse=[{
		name:'Select Course',
		value:''
		}];
	$scope.course_name=$scope.listOfCourse[0];
	var crsid={'dept_id':$scope.dept_name.value};
	$http({
		method: 'POST',
		url:"php/course/getPrincipalMyPlanCourseData.php",
		data:crsid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.course_name,'value':obj.course_id};
			$scope.listOfCourse.push(data);
		});
	},function errorCallback(response) {
		
	});
	$scope.listFaculty=null;
	$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
	$scope.faculty_name=$scope.listFaculty[0];
	
	$http({
		method: 'POST',
		url: "php/princpaltime/getPrincipalWDSFaculty.php",
		data:crsid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response) {
		if(response.data=="null"){
			$scope.listFaculty=null;
		$scope.listFaculty=[];
		$scope.listFaculty=[{
		name: 'Select Faculty',
		value: ''
	}]
		}else{
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.user_name);									
			var Faculty={'name':obj.role+" : "+obj.user_name , 'value':obj.user_id};
			$scope.listFaculty.push(Faculty);
		});
		}
	},function errorCallback(response) {
		
	});
	
	}
	
	$scope.listOfLession=[{
		name:'Select Lession plan',
		value:''
	}
	];
	
	$scope.new_unit_name="";
	$scope.viewUnitData=[];
	$scope.viewPlanData=[];
	$scope.listUnitData=[];
	$scope.listUnitData=[{
		name:'Select Unit',
		value:''
	}
	];
	$scope.unit_name=$scope.listUnitData[0];
	
	
	$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Academic Year',
		value: ''
	}]
	$scope.session_name=$scope.listSession[0];
	
	
	/*$scope.listSession=[];
	$scope.listSession=[{
		name: 'Select Subject',
		value: ''
	}]*/
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	$scope.listOfdept=[{
		name:'Select Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfdept[0];
	
	$scope.listSubject=[{
			name: 'Select Subject',
			value: ''
		}]
	$http({
		method: 'GET',
		url:"php/stream/readPricpalStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	$scope.sub_name = $scope.listSubject[0];
	
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	
	
		$http({
		method:'GET',
		url:"php/userplan/getLession.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.lession_name,'value':obj.lession_id};
			$scope.listOfLession.push(data);
		});
		},function errorCallback(response) {
		});



		$scope.lession=$scope.listOfLession[0];
		
	
		
	
	$scope.noSemesters=[{
		name:'Select Semester',
		value:''
	}];
	
	$scope.semester=$scope.noSemesters[0];
	
	$scope.listOfSubject=[{
		name:'Select Subject',
		value:''
	}];
	
	
	$scope.subject_name=$scope.listOfSubject[0];
	
	$scope.listOfSection=[{
		name:'Select section',
		value:''
	}];
	$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSection.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.section=$scope.listOfSection[0];
	$scope.noSemesters=[];
	
	
	$scope.selectedCourse=function(id){
			//alert("::"+id);
			focusStreamField.removeBorderColor(id);
				
		     $scope.noSemesters=null;
			 $scope.noSemesters=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'course_id':$scope.course_name.value};
			 $http({
				 method:'POST',
				 url:"php/course/getCollegeSemester.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				 if($scope.course_name.name==response.data[0].course_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.noSemesters.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	
	
	$scope.selectedSemester=function(id){
		focusStreamField.removeBorderColor(id);
		
		$scope.listOfSubject=null;
		$scope.listOfSubject=[];
		$scope.listOfSubject=[{
			name: 'Select Subject',
			value: ''
		}]
		var strmid=$scope.stream_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		//alert("semester_id::"+semester_id);
		var userdata={'stream_id':strmid,'dept_id':$scope.dept_name.value,'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getMyPlanSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			var Subject={'name':obj.type+" : "+obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSubject.push(Subject);
			
			});
		},function errorCallback(response) {
		});	
		
	}
	
	/*.........START GET DEPARTMENT FROM UNIT PLAN TABLE...............*/
	$http({
		method:'POST',
		url:"php/princpalplan/getDeptforUnitPlan.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('wds',response.data);
		angular.forEach(response.data,function(clg){
			var dept={'name':clg.dept_name,'value':clg.dept_id};
			$scope.listOfdept.push(dept);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'POST',
		url:"php/princpalplan/getDeptforUnitPlan.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('wds',response.data);
		angular.forEach(response.data,function(clg){
			var dept={'name':clg.dept_name,'value':clg.dept_id};
			$scope.listOfFillterdept.push(dept);
		});
	},function errorCallback(response) {
	});
	$scope.getRecordUsingDept=function(){
		if($scope.deptName.value!=''){
		var userdata={'dept_id':$scope.deptName.value};
		$http({
		method:'POST',
		url:"php/principalWDS/readAllCompletedDeptUnitData.php",
		data:userdata,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.showTable=true;
		$scope.viewUnitData=response.data;
		//console.log('respon',response.data);
	},function errorCallback(response) {
	});
		}else{
			$scope.showTable=false;
		}
	}
	$scope.attchColgDept=function(dobj){
		$http({
		method:'POST',
		url:"php/princpalplan/getDeptforUnitPlan.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('wds',response.data);
		angular.forEach(response.data,function(clg){
			var dept={'name':clg.dept_name,'value':clg.dept_id};
			$scope.listOfFillterdept.push(dept);
			if(clg.dept_id==dobj.dept_id){
			$scope.deptName.value=dobj.dept_id;
			$scope.viewUnitData=[];
			$scope.viewUnitData.push(dobj);
			$scope.showTable=true;
			}
		});
	},function errorCallback(response) {
	});
	}
	$scope.showYourListAfterAdd=function(dobj){
		$scope.showFirstForm=true;
			var dataset={'dept_id':dobj.dept_id,'user_id':dobj.user_id};
			$http({
			method:'POST',
			url:'php/principalWDS/readListOfWDSData.php',
			data:dataset,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.viewIncompletePlanData=response.data;
			//console.log('table',$scope.viewIncompletePlanData.length);
			$scope.displayTableList=true;
		},function errorCallback(response) {
		});
	}
	
	/*.........END GET DEPARTMENT FROM UNIT PLAN TABLE...............*/
	/*.........Start Global variable declaration...........*/
	
	var unitid='';
	var planid='';
	var facultyid='';
	var plan_name='';
	
	
	/*..........END Global variable declaration...........*/
	
	/*$http({
		method:'GET',
		url:"php/principalWDS/readAllCompletedUnitData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response.data);
		$scope.viewUnitData=response.data;
	},function errorCallback(response) {
	});*/
	
	
	
	$scope.addPlanData=function(){
		//alert("::"+$scope.buttonName);
		if($scope.buttonName=="Add"){
		var today=new Date();
		 if($scope.date==null || $scope.date=="" ){
			alert('Please select completed date');
			focusStreamField.borderColor('date');
		}else if(today < new Date($scope.date.split("-").reverse().join(","))){
			alert('Please select todays date or previous date');
			focusStreamField.borderColor('date');
		}else if($scope.time_slot.value==""){
			alert('Please select time slot');
			focusStreamField.borderColor('time_slot');
		}else{
			
			var dataString = "user_id="+facultyid+"&unit_id="+unitid+"&plan_id="+planid+"&date="+$scope.date+"&time_id="+$scope.time_slot.value+"&status="+$scope.status_name.value+"&plan_name="+plan_name;
			//alert("dataString:"+dataString);
			$.ajax({ 
			type: "POST",url: "php/principalWDS/addFacultyCompletedWDSData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					alert("New completed plan added successfully...");
					//$state.go('principal.plan.facultywds',{}, { reload: true });
					if(dobj.dept_id != ''){
					$scope.attchColgDept(dobj);
					}
					$scope.showYourListAfterAdd(dobj)
				}
			} 
			});
			
			
			
			
		}
		
		
		}
		if($scope.buttonName=="Update"){
			//alert("aaaaaaa");
			if($scope.date==null){
			alert('Please select date');
			}else if($scope.plan.value==""){
			alert('Please add  plan');
			}else if($scope.topic==null){
			alert('Please add topic');
			}else{
			
			var dataString = "unit_plan_id="+temp_unit_id+"&plan_id="+id+"&date="+$scope.date+"&lession_plan="+$scope.plan.name+"&topic="+$scope.topic;
			//alert("::"+dataString);
			
			$.ajax({ 
			type: "POST",url: "php/userplan/updatePlanData.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					var updatedata={'unit_id':temp_unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						console.log('res',response.data);
						$scope.viewPlanData=response.data;
						//document.getElementById("unit_list").style.display = "none";
						//document.getElementById("plan_list").style.display = "block";
					},function errorCallback(response) {
						
						
					});
					
					$scope.date = null;
					$scope.plan = null;
					$scope.topic = null;
					$scope.clearPlanData();
					alert("Plan updated successfully...");
				
				}
			} 
			});
			
			
			
			
			
		}
		}
	}
	
	$scope.addUnit=function(){
		if($scope.session_name.value==''){
			alert('Please select Academic Year');
		}else if($scope.course_name.value==''){
			alert('Please select the course name');
		}else if($scope.semester.value==''){
			alert('Please select the semester');
		}else if($scope.subject_name.value==''){
			alert('Please select the subject name');
		}else if($scope.section.value==''){
			alert('Please select the section');
		}else{
				//alert("Add Unit");
				document.getElementById("addunit").style.display ="block";
			}
	}
	
	
	$scope.getPlanData=function(id){
			$scope.clearField(id);
		
			$scope.listPlanData=[];
			$scope.listPlanData=[{
				name:'Select Plan',
				value:''
			}
			];
	
			var updatedata={'unit_id':$scope.unit_name.value };
			//alert("aa::"+$scope.unit_name.value);
			$http({
				method:'POST',
				url:"php/userwds/getPlanName.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
				//alert("::"+obj.plan);									
				var Session={'name':obj.plan , 'value':obj.plan_id};
					$scope.listPlanData.push(Session);
				});
				
			},function errorCallback(response) {
				
				
			});
	
	
	}
	
	
	
	$scope.getPlanDetails=function(id){
			$scope.clearField(id);
			//alert("aaaa");
			var updatedata={'unit_id':$scope.unit_name.value , 'plan_id':$scope.plan_name.value };
			$http({
				method:'POST',
				url:"php/principalWDS/getPlanInfo.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data, function(obj){
					$scope.date = obj.date;
				});
				
			},function errorCallback(response) {
				
				
			});
	
	
	}
	
	
	
	$scope.checkTableData=function(id){
		
		focusStreamField.removeBorderColor(id);
		
		if($scope.session_name.value != "" )
		{
			$scope.listUnitData=[];
			$scope.listUnitData=[{
				name:'Select Unit',
				value:''
			}
			];
	
			var userid={'dept_id':$scope.dept_name.value,'user_id':$scope.faculty_name.value};
				
			$http({
				method:'POST',
				url:"php/principalWDS/getFacultyWDSUnitName.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res unit',response.data);
				angular.forEach(response.data, function(obj){
				var Session={'name':obj.unit_name , 'value':obj.unit_plan_id};
					$scope.listUnitData.push(Session);
				});
				
			},function errorCallback(response) {
				
				
			});
		}
		
	}
	
	
	$scope.closeUnitDialog=function(){
			document.getElementById("addunit").style.display ="none";
	}
	
	$scope.addNewUnitName=function(){

		//alert("::"+$scope.new_unit_name);
		var unit_name = $scope.new_unit_name;
		//alert("unit_name::"+unit_name+"::");
		if(unit_name == "")
		{
			alert("Add Unit Name");
			return;
		}
		var session_id = $scope.session_name.value;
		var course_id = $scope.course_name.value;
		var semester_id = $scope.semester.value;
		var subject_id = $scope.subject_name.value;
		var section_id = $scope.section.value;
		
		
		var dataString = "session_id="+session_id+"&course_id="+course_id+"&semester_id="+semester_id+"&subject_id="+subject_id+"&section_id="+section_id+"&unit_name="+unit_name;
			
			$.ajax({ 
			type: "POST",url: "php/userplan/addUnitName.php" ,data: dataString,cache: false,
			success: function(html)
			{
				var dobj=jQuery.parseJSON(html);
				//alert(dobj.result);
				if(dobj.result == 0)
				{
					alert(dobj.error);
				}
				else
				{
					//$scope.checkTableData();
					document.getElementById("addunit").style.display ="none";
					
					$scope.viewUnitData= null;
					$http({
					method:'GET',
					url:"php/userplan/readUnitData.php",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
					},function errorCallback(response) {
					});
					
					
					
					
				}
			} 
			});
	}
	
	
	
	$scope.editPlanData=function(pid,unit_id){
		id=pid;
		temp_unit_id = unit_id;
		var planid={'plan_id':id};
		$http({
			method:'POST',
			url:"php/userplan/editPlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
			$scope.date=response.data[0].date;
			$scope.plan=response.data[0].plan;
			$scope.topic=response.data[0].topic;
			$scope.user_readonly= true;
			$scope.showCancel =  true;
			$scope.buttonName="Update";
		
		},function errorCallback(response) {
		});
		
	}
	
	
			
	$scope.viewPlanData=null;
	$scope.viewPlanData=[];
	$http({
			method:'GET',
			url:"php/userwds/readCompletePlanData.php",
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('res',response.data);
			$scope.viewPlanData=response.data;
			document.getElementById("unit_list").style.display = "none";
			document.getElementById("plan_list").style.display = "block";
		},function errorCallback(response) {
			
				
	});

	
	$scope.showUnitData=function(){
		document.getElementById("unit_list").style.display = "block";
		$scope.showTable=true;
		document.getElementById("plan_list").style.display = "none";
	
	}
	
	$scope.showCompletedPlanData=function(unit_id){
		//alert("unit_id:"+unit_id);
		
		var updatedata={'unit_id':unit_id};
		
		$scope.viewPlanData=null;
		$scope.viewPlanData=[];
			
			$http({
				method:'POST',
				url:"php/userwds/getCompletedPlanData.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('res',response.data);
				$scope.viewPlanData=response.data;
				document.getElementById("unit_list").style.display = "none";
				$scope.showTable=false;
				document.getElementById("plan_list").style.display = "block";
			},function errorCallback(response) {
				
				
			});
	
	}
	
	
	
	$scope.verifiedPlanData=function(id){
		
		//alert("::"+value);
		if(confirm("Would you like to approve ?"))
		{
			value = 1;
			var updatedata={'status':value,'unit_id':id };
			$http({
				method:'POST',
				url:"php/hod/updateUnitStatus.php",
				data:updatedata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				
				$http({
					method:'GET',
					url:"php/principalWDS/readAllCompletedUnitData.php",
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					//console.log('res',response.data);
					$scope.viewUnitData=response.data;
				},function errorCallback(response) {
				});
				
				
				//$state.go('hod.wds',{}, { reload: true }); ///chinu
				
			},function errorCallback(response) {
			});
		}
		
	}
	
	
	
	$scope.deletePlanData=function(pid,unit_id){
		id=pid;
		var planid={'plan_id':id , 'unit_id':unit_id};
		var deleteUser = $window.confirm('Are you sure  to delete?');
		if(deleteUser){
			$http({
			method: 'POST',
			url: "php/userplan/deletePlanData.php",
			data:planid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			
				var updatedata={'unit_id':unit_id};
		
					$scope.viewPlanData=null;
					$scope.viewPlanData=[];
			
					$http({
						method:'POST',
						url:"php/userplan/readPlanData.php",
						data:updatedata,
						headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
					}).then(function successCallback(response){
						//console.log('res',response.data);
						$scope.viewPlanData=response.data;
					},function errorCallback(response) {
						
						
					});
		
					
		
		
		},function errorCallback(response) {
			//alert(response.data);
			//$state.go('user.plan',{}, { reload: true });
		});
		}
	}
	$scope.clearPlanData=function(){
		$scope.displayDepartment=true;
		$scope.readdept=false;
		$scope.disdept=false;
		$scope.displayAcademic=false;
		$scope.displayunitname=false;
		$scope.displaylessionplan=false;
		$scope.displayinfo=false;
		$scope.displayFaculty=true
		$scope.user_readonly= false;
		$scope.displayDate=false;
		$scope.displayTimeSlot=false;
		$scope.displayStatus=false;
		$scope.displayAdd=false;
		$scope.showCancel =  false;
		$scope.showYourList();
		//$scope.buttonName ="Add";
		//$state.go('dashboard.plan.facwds',{}, { reload: true });
	}
	
	$scope.listSearchSession=[];
	$http({
		method: 'GET',
		url: "php/plan/readSession.php",
	}).then(function successCallback(response) {
		angular.forEach(response.data, function(obj){
			//alert("::"+obj.session);									
			var Session={'name':obj.session , 'value':obj.session_id};
			$scope.listSearchSession.push(Session);
		});
	},function errorCallback(response) {
		
	});
	$scope.listOfSearchCourse=[];
	$http({
		method:'GET',
		url:"php/userplan/getCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj1){
			var data={'name':obj1.short_name,'value':obj1.course_id};
			$scope.listOfSearchCourse.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.selectedSearchCourse=function(){
		     $scope.listOfSearchSemester=null;
			 $scope.listOfSearchSemester=[];
			 var key=['I','II','III','IV','V','VI','VII','VIII','IX','X'];
			 var userid={'id':$scope.courseSearch.value};
			 //console.log('cid',userid);
			 $http({
				 method:'POST',
				 url:"php/userplan/getSemesterData.php",
				 data: userid,
				 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			 }).then(function successCallback(response){
				// console.log('cid res',response.data);
				 if($scope.courseSearch.name==response.data[0].short_name){
					 for(var i=0;i<response.data[0].semester;i++){
						 var val=i+1;
						 var rom_val=key[i];
						 var sem={'name':rom_val,'value':val};
						 $scope.listOfSearchSemester.push(sem);
					 }
				 }
			 },function errorCallback(response) {
				 $scope.noSemesters=null;
			 });
	}
	$scope.selectedSearchSemester=function(){
		$scope.listOfSearchSubject=null;
		$scope.listOfSearchSubject=[];
		
		var course_id = $scope.courseSearch.value;
		var semester_id = $scope.semesterSearch.value;
		//alert("semester_id::"+semester_id);
		var userdata={'course_id':course_id,'semester_id':semester_id};
		$http({
			method: 'POST',
			url: "php/userplan/getSubjectData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//console.log('edited data',response);
			angular.forEach(response.data, function(obj){
			//alert("::"+obj.subject_name);									
			var Subject={'name':obj.subject_name , 'value':obj.subject_id};
			$scope.listOfSearchSubject.push(Subject);
			
			});
			
			
			
		},function errorCallback(response) {
		});	
	}
	$scope.listOfSearchSection=[];
	$http({
		method:'GET',
		url:"php/userplan/getSectionData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.section_name,'value':obj2.section_id};
			$scope.listOfSearchSection.push(data);
		});
	},function errorCallback(response) {
	});
	
	$scope.listOfSearchUnit=[];
	$http({
				method:'POST',
				url:"php/userplan/getFilterUnitData.php",
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				console.log('unit',response.data);
				angular.forEach(response.data,function(obj1){
					var unitvalue={'name':obj1.unit_name,'value':obj1.unit_name};
					$scope.listOfSearchUnit.push(unitvalue)
				})
			},function errorCallback(response) {
			});
	
	$scope.clearField=function(id){
		//alert("Clear:"+id);
		focusStreamField.removeBorderColor(id);
	}
	
	$scope.removeBorder=function(id){
		if($scope.colg_name != ''){
			focusStreamField.removeBorderColor(id);
		}
	}
	$scope.GetDeptData=function(id){
		focusStreamField.removeBorderColor(id);
		$scope.listOfdept=null;
		$scope.listOfdept=[{
		name:'Select Department',
		value:''
	    }]
	    $scope.dept_name=$scope.listOfdept[0];
		
		var courseid={'stream_id':$scope.stream_name.value}
		$http({
			method:'POST',
			url:"php/deptsubject/readPrincipalDeptValue.php",
			data:courseid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			angular.forEach(response.data,function(obj){
				var data={'name':obj.dept_name,'value':obj.dept_id};
				$scope.listOfdept.push(data);
			});
		},function errorCallback(response) {
		});
	}
	$scope.showYourList=function(id){
		focusStreamField.removeBorderColor(id);
		if($scope.dept_name.value==null || $scope.dept_name.value==''){
			alert('Please select department');
			focusStreamField.borderColor('dept_name');
		}else if($scope.faculty_name.value==null || $scope.faculty_name.value==''){
			alert('Please select faculty');
			focusStreamField.borderColor('faculty_name');
		}else{
			$scope.showFirstForm=true;
			var dataset={'dept_id':$scope.dept_name.value,'user_id':$scope.faculty_name.value};
			$http({
			method:'POST',
			url:'php/principalWDS/readListOfWDSData.php',
			data:dataset,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.viewIncompletePlanData=response.data;
			//console.log('table',$scope.viewIncompletePlanData.length);
			$scope.displayTableList=true;
		},function errorCallback(response) {
		});
			
		}
	}
	$scope.displaySecondForm=function(colg_id,unit_id,plan_id,user_id,plan,unitName,session,sec,subject,semester,course,date){
		unitid=unit_id;
		planid=plan_id;
		facultyid=user_id;
		plan_name=plan;
		$scope.displayDepartment=true;
		$scope.readdept=true;
		$scope.disdept=true;
		$scope.displayAcademic=true;
		$scope.AcademicName=session;
		$scope.Academicdis=true;
		$scope.readAcademic=true;
		$scope.displayunitname=true;
		$scope.unitName=unitName;
		$scope.readUnit=true;
		$scope.unitdis=true;
		$scope.displaylessionplan=true;
		$scope.planName=plan;
		$scope.readPlan=true;
		$scope.plandis=true;
		$scope.displayinfo=true;
		$scope.infoName=course+'/'+sec+'/'+subject+'/'+semester;
		$scope.readinfo=true;
		$scope.infodis=true;
		$scope.displayFaculty=true;
		$scope.user_readonly=true;
		$scope.displayDate=true;
		$scope.date=date;
		$scope.displayTimeSlot=true;
		$scope.displayStatus=true;
		$scope.displayAdd=true;
		$scope.buttonName="Add";
		$scope.clearButtonName="Cancel";
		$scope.showCancel=true;
		$scope.listTimeData=null;
	    $scope.listTimeData=[{
		name:'Select Time Slot',
		value:''
	   }
	   ];
	   $scope.time_slot=$scope.listTimeData[0];
	
	$http({
		method:'GET',
		url:"php/userwds/getTimeSlot.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj2){
			var data={'name':obj2.time_slot,'value':obj2.id};
			$scope.listTimeData.push(data);
		});
	},function errorCallback(response) {
	});
		
	}
	
});
	
stream.factory('focusStreamField',function($timeout,$window){
	return{
		borderColor:function(id){
			var element = $window.document.getElementById(id);
			 if(element){
				 element.focus();
			     element.style.borderColor = "red";
			 }
		},
		removeBorderColor:function(id){
			$timeout(function(){
				var element = $window.document.getElementById(id);
				if(element){
					 element.style.borderColor = "#cccccc";
				}
			})
		}
	}
});