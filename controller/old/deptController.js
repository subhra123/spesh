var dashboard=angular.module('Channabasavashwara');
dashboard.controller('deptController',function($scope,$http,$location,$state,focusDeptField){
	$scope.buttonName="Add";
	var id='';
     //var id=gup( "d_i" );
	 $scope.inputType="password";
	 $scope.hideShowPassword=function(){
		 if($scope.inputType=='password'){
			 $scope.inputType="text";
		 }else{
			 $scope.inputType="password";
		 }
	 }
	 $scope.hidePassAfterLeave=function(){
		 $scope.inputType="password";
	 }
	 $http({
		method: 'GET',
		url: "php/department/readDeptData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.deptData=response.data;
	},function errorCallback(response) {
		
	});
	$scope.addDepartmentData=function(billdata){
		if(billdata.$valid){
		if($scope.buttonName=='Add'){
			if($scope.colg_name.value==null || $scope.colg_name.value==''){
				alert('Please select your college...');
				focusDeptField.borderColor('colg_name');
			}else if($scope.stream_name.value==null || $scope.stream_name.value==''){
			alert('Stream Name field could not be blank...');
			focusDeptField.borderColor('stream_name');
		}else if($scope.deptname==null || $scope.deptname==''){
			alert('Department Name field could not be blank...');
			focusDeptField.borderColor('deptname');
		}else if($scope.dept_short_name==null || $scope.dept_short_name==''){
			alert('Department code field could not be blank...');
			focusDeptField.borderColor('deptshname');
		}else if($scope.dept_login_name==null || $scope.dept_login_name==''){
			alert('User name field could not be blank...');
			focusDeptField.borderColor('deptlogname');
		}else if($scope.password==null || $scope.password=='' ){
			alert('Password field could not be blank...');
			focusDeptField.borderColor('deptpass');
		}else{
		var userdata={'dept_name':$scope.deptname,'short_name':$scope.dept_short_name,'login_name':$scope.dept_login_name,'password':$scope.password,'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value};
		//console.log('userdata',userdata);
		$http({
			method: 'POST',
			url: "php/department/addDeptData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('profile',response);
			alert(response.data['msg']);
			if(response.data['result'] == 1)
			{
				$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
			}
			
		},function errorCallback(response) {
			alert(response.data['msg']);
			if($scope.deptname==response.data.dept_name){
				focusDeptField.borderColor('deptname');
			}else if($scope.dept_short_name==response.data.short_name){
				focusDeptField.borderColor('deptshname');
			}else{
			//$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
			}
		})
		
		}
		}
		
		if($scope.buttonName=='Update'){
			console.log('update',id);
			if($scope.colg_name.value==null || $scope.colg_name.value==''){
				alert('Please select your college...');
				focusDeptField.borderColor('colg_name');
			}else if($scope.stream_name.value==null || $scope.stream_name.value==''){
			alert('Stream Name field could not be blank...');
			focusDeptField.borderColor('stream_name');
		}else if($scope.deptname==null || $scope.deptname==''){
			alert('Department Name field could not be blank...');
			focusDeptField.borderColor('deptname');
		}else if($scope.dept_short_name==null || $scope.dept_short_name==''){
			alert('Department code field could not be blank...');
			focusDeptField.borderColor('deptshname');
		}else if($scope.dept_login_name==null || $scope.dept_login_name==''){
			alert('User name field could not be blank...');
			focusDeptField.borderColor('deptlogname');
		}else{
			var updatedata={'dept_name':$scope.deptname,'short_name':$scope.dept_short_name,'login_name':$scope.dept_login_name,'password':$scope.password,'dept_id':id,'colg_name':$scope.colg_name.value,'stream_name':$scope.stream_name.value};
			$http({
			method: 'POST',
			url: "php/department/updateDeptData.php",
			data: updatedata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			if($scope.deptname==response.data.dept_name){
				focusDeptField.borderColor('deptname');
			}else if($scope.dept_short_name==response.data.short_name){
				focusDeptField.borderColor('deptshname');
			}else{
			//$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
			}
		});
		}
		}
		}else{
			alert('This is an invalid form');
		}
	}
	
	
	$scope.getDeptData=function(did){
		id=did;
		var userdata={'userid':id};
		
		$http({
			method: 'POST',
			url: "php/department/editDeptData.php",
			data: userdata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			console.log('edited data',response.data[0].stream_id);
			$scope.colg_name.value=response.data[0].colg_id;
			var value=response.data[0].stream_id;
			var colgid={'colg_name':$scope.colg_name.value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
					$scope.stream_name.value=value;
				});
			},function errorCallback(response) {
			});
			//$scope.colg_name.value=response.data[0].colg_id;
			$scope.getcode(response.data[0].colg_id);
			$scope.deptname=response.data[0].dept_name;
			$scope.dept_short_name=response.data[0].short_name;
			$scope.dept_login_name=response.data[0].login_name;
			$scope.readcolg=true;
			$scope.readstrm=true;
			$scope.colgdis=true;
			$scope.strmdis=true;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.showCancel=true;
			
		},function errorCallback(response) {
		});
	}
	
	$scope.deleteDeptData=function(did){
		if(confirm("Are you sure want to delete?"))
		{
			id=did;
			var userdata={'userid':id};
			//alert("Confirm");
			$http({
				method: 'POST',
				url: "php/department/deleteDeptData.php",
				data: userdata,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
				//console.log('edited data',response);
				//$scope.deptname=response.data[0].dept_name;
				//$scope.dept_short_name=response.data[0].short_name;
				//$scope.buttonName="Update";
				//$scope.loginname=response.data[0].login_name;
			},function errorCallback(response) {
			});
		}
	}
	$scope.listOfCollege=[{
		name:'Select your college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
			//$scope.college_code=obj.code;
		});
	},function errorCallback(response) {
	});
	$scope.clearField=function(id){
		focusDeptField.clearBorderColor(id);
	}
	$scope.removeBorder=function(id){
		if($scope.colg_name.value != ''){
			focusDeptField.clearBorderColor(id);
			$scope.listOfStream=null;
			$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
			var colgid={'colg_name':$scope.colg_name.value};
			$http({
				method:'POST',
				url:"php/department/readStreamData.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				angular.forEach(response.data,function(obj){
					var data={'name':obj.stream_name,'value':obj.stream_id};
					$scope.listOfStream.push(data);
				});
			},function errorCallback(response) {
			});
			$http({
				method:'POST',
				url:"php/department/readCollegeCode.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				$scope.college_code=response.data[0].code;
			},function errorCallback(response) {
			});
		}
	}
	$scope.listOfStream=[{
		name:'Select your stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$scope.listOfSearchdept=[];
	$scope.getDept=function(){
		$scope.listOfSearchdept=null;
		$scope.listOfSearchdept=[];
		var colgid={'colg_id':$scope.colgSearch.value}
	$http({
		method:'POST',
		url:"php/department/readDeptValue.php",
		data:colgid,
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.dept_name,'value':obj.dept_id};
			$scope.listOfSearchdept.push(data);
			 
		});
	},function errorCallback(response) {
	});
	}
	$scope.updateField=function(id){
		//focusDeptField.clearBorderColor(id);
		$scope.dept_login_name  =  $scope.college_code+"_"+$scope.dept_short_name;
		
	}
	$scope.calceldata=function(){
		$state.go('dashboard.deptmanagement.dept',{}, { reload: true });
	}
	$scope.removeStream=function(mod,id){
		//console.log('mod',mod,id);
		if(mod.value != ''){
			focusDeptField.clearBorderColor(id);
		}
	}
	$scope.getcode=function(value){
		var colgid={'colg_name':value}
		$http({
				method:'POST',
				url:"php/department/readCollegeCode.php",
				data:colgid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				$scope.college_code=response.data[0].code;
			},function errorCallback(response) {
			});
	}
});
dashboard.factory('focusDeptField',function($timeout, $window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});