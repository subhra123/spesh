var Admin=angular.module('Channabasavashwara',['ui.router', '720kb.datepicker','ngMessages','ngCapsLock','ui.bootstrap']);
Admin.config(function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/');
	$stateProvider
	 .state('/', {
            url: '/',
            templateUrl: 'dashboardview/login.html',
			controller: 'loginController'
        })
		.state('dashboard', {
            url: '/dashboard',
			templateUrl: 'dashboardview/dashboard.html',
			controller: 'dashboardController'
        })
		.state('dashboard.profile', {
        url: '/profile',
        templateUrl: 'dashboardview/profile.html',
        controller: 'profileController'
    })
	.state('dashboard.deptmanagement', {
        url: '/dept',
        templateUrl: 'dashboardview/deptmanagement.html',
        controller: 'deptmanagementController'
    })
	.state('dashboard.deptmanagement.stream', {
        url: '/stream',
        templateUrl: 'dashboardview/stream.html',
        controller: 'streamController'
    })
	.state('dashboard.deptmanagement.course', {
        url: '/course',
        templateUrl: 'dashboardview/course.html',
        controller: 'resourcecourseController'
    })
	.state('dashboard.deptmanagement.sub', {
        url: '/subject',
        templateUrl: 'dashboardview/subject.html',
        controller: 'deptsubjectController'
    })
	.state('dashboard.deptmanagement.dept', {
        url: '/dept',
        templateUrl: 'dashboardview/dept.html',
        controller: 'deptController'
    })
	.state('dashboard.princpal', {
        url: '/princpal',
        templateUrl: 'dashboardview/princpal.html',
        controller: 'princpalController'
    })
	.state('dashboard.dept_head', {
        url: '/dept_head',
        templateUrl: 'dashboardview/depthead.html',
        controller: 'deptheadController'
    })
	.state('dashboard.user', {
        url: '/user',
        templateUrl: 'dashboardview/user.html',
        controller: 'managementUserController'
    })
	.state('dashboard.user.usermanagement', {
        url: '/usermanagement',
        templateUrl: 'dashboardview/usermangement.html',
        controller: 'managementController'
    })
	.state('dashboard.user.role', {
        url: '/role',
        templateUrl: 'dashboardview/role.html',
        controller: 'roleController'
    })
	.state('dashboard.editprofile', {
        url: '/editprofile',
        templateUrl: 'dashboardview/editprofile.html',
        controller: 'adminProfileController'
    })
	.state('dashboard.plan', {
        url: '/plan',
        templateUrl: 'dashboardview/plan.html',
        controller: 'planmanagementController'
    })
	.state('dashboard.plan.contime', {
        url: '/TimeTable',
        templateUrl: 'homeview/timeTable.html',
        controller: 'timeTableController'
    })
	.state('dashboard.plan.factime', {
        url: '/FacultyTimeTable',
        templateUrl: 'homeview/facultytimeTable.html',
        controller: 'facultyTimeTableController'
    })
	.state('dashboard.plan.facplan', {
        url: '/FacultyPlan',
        templateUrl: 'homeview/plan.html',
        controller: 'planController'
    })
	.state('dashboard.plan.facwds', {
        url: '/WDS',
        templateUrl: 'homeview/wds.html',
        controller: 'wdsController'
    })
	/*.state('dashboard.stream', {
        url: '/stream',
        templateUrl: 'dashboardview/stream.html',
        controller: 'streamController'
    })*/
	.state('dashboard.resource', {
        url: '/resource',
        templateUrl: 'dashboardview/resource.html',
        controller: 'resourceController'
    })
	.state('dashboard.res', {
        url: '/res',
        templateUrl: 'dashboardview/res.html',
        controller: 'resController'
    })
	.state('dashboard.res.userrole', {
        url: '/userrole',
        templateUrl: 'dashboardview/userrole.html',
        controller: 'resourceuserroleController'
    })
	.state('dashboard.res.course', {
        url: '/course',
        templateUrl: 'dashboardview/course.html',
        controller: 'resourcecourseController'
    })
	.state('dashboard.res.class', {
        url: '/class',
        templateUrl: 'dashboardview/class.html',
        controller: 'resourceclassController'
    })
	.state('dashboard.res.section', {
        url: '/section',
        templateUrl: 'dashboardview/section.html',
        controller: 'resourcesectionController'
    })
	.state('dashboard.res.session', {
        url: '/session',
        templateUrl: 'dashboardview/session.html',
        controller: 'resourceSessionController'
    })
	.state('dashboard.res.unit', {
        url: '/unit',
        templateUrl: 'dashboardview/unit.html',
        controller: 'resourceunitController'
    })
	.state('dashboard.res.venue', {
        url: '/venue',
        templateUrl: 'dashboardview/venue.html',
        controller: 'resourcevenueController'
    })
	.state('home',{
		url: '/home',
		templateUrl: 'homeview/home.html',
		controller: 'homeController'
	})
	.state('home.course', {
        url: '/course',
        templateUrl: 'homeview/course.html',
        controller: 'coursecontroller'
    })
	.state('home.subject', {
        url: '/subject',
        templateUrl: 'homeview/subject.html',
        controller: 'subjectcontroller'
    })
	.state('home.hod', {
        url: '/hod',
        templateUrl: 'homeview/hod.html',
        controller: 'hodcontroller'
    })
	.state('home.usermanagement', {
        url: '/user',
        templateUrl: 'homeview/usermangement.html',
        controller: 'homemanagementController'
    })
	.state('home.role', {
        url: '/role',
        templateUrl: 'homeview/role.html',
        controller: 'homeroleController'
    })
	.state('home.time', {
        url: '/TimeTable',
        templateUrl: 'homeview/timeTable.html',
        controller: 'timeTableController'
    })
	.state('home.facultytime', {
        url: '/FacultyTimeTable',
        templateUrl: 'homeview/facultytimeTable.html',
        controller: 'facultyTimeTableController'
    })
	.state('home.plan', {
        url: '/FacultyPlan',
        templateUrl: 'homeview/plan.html',
        controller: 'planController'
    })
	.state('home.wds', {
        url: '/WDS',
        templateUrl: 'homeview/wds.html',
        controller: 'wdsController'
    })
	.state('home.profile', {
        url: '/profile',
        templateUrl: 'homeview/profile.html',
        controller: 'homeprofileController'
    })
	.state('user',{
		url: '/user',
		templateUrl: 'userview/user.html',
		controller: 'userController'
	})
	
	.state('user.mytimetable',{
		url: '/mytimetable',
		templateUrl: 'userview/mytimetable.html',
		controller: 'userTimeTableController'
	})
	
	.state('user.myplan',{
		url: '/myplan',
		templateUrl: 'userview/myplan.html',
		controller: 'userPlanController'
	})
	
	.state('user.mywds',{
		url: '/mywds',
		templateUrl: 'userview/mywds.html',
		controller: 'userWDSController'
	})
	.state('user.profile',{
		url: '/profile',
		templateUrl: 'userview/profile.html',
		controller: 'userProfileController'
	})
	
	<!-------------- PRINCIPAL START ------------------->
	.state('principal',{
		url: '/principal',
		templateUrl: 'princpalview/princpal.html',
		controller: 'princpalHomeController'
	})
	.state('principal.dept', {
        url: '',
        templateUrl: 'princpalview/dept.html',
        controller: 'pricipalDeptController'
    })
	.state('principal.dept.stream', {
        url: '/stream',
        templateUrl: 'princpalview/stream.html',
        controller: 'pricipalDeptStreamController'
    })
	
	.state('principal.dept.department', {
        url: '/department',
        templateUrl: 'princpalview/department.html',
        controller: 'pricipalDeptDepartmentController'
    })
	
	.state('principal.dept.course', {
        url: '/course',
        templateUrl: 'princpalview/course.html',
        controller: 'pricipalDeptCourseController'
    })
	
	.state('principal.dept.subject', {
        url: '/subject',
        templateUrl: 'princpalview/subject.html',
        controller: 'pricipalDeptSubjectController'
    })
	.state('principal.resourse', {
        url: '/resourse',
        templateUrl: 'princpalview/res.html',
        controller: 'resPrincpalController'
    })
	.state('principal.resourse.userrole', {
        url: '/userrole',
        templateUrl: 'princpalview/userrole.html',
        controller: 'resourcePrincpaluserroleController'
    })
	.state('principal.resourse.class', {
        url: '/class',
        templateUrl: 'princpalview/class.html',
        controller: 'resourcePrincpalclassController'
    })
	.state('principal.resourse.section', {
        url: '/section',
        templateUrl: 'princpalview/section.html',
        controller: 'resourcePrincpalsectionController'
    })
	.state('principal.resourse.session', {
        url: '/session',
        templateUrl: 'princpalview/session.html',
        controller: 'resourcePrincpalSessionController'
    })
	.state('principal.resourse.unit', {
        url: '/unit',
        templateUrl: 'princpalview/unit.html',
        controller: 'resourcePrincpalunitController'
    })
	.state('principal.resourse.venue', {
        url: '/venue',
        templateUrl: 'princpalview/venue.html',
        controller: 'resourcePrincpalvenueController'
    })
	.state('principal.planmanagement', {
        url: '/unit',
        templateUrl: 'princpalview/planmanagement.html',
        controller: 'planmanagementPrincpalController'
    })
	.state('principal.user', {
        url: '/user',
        templateUrl: 'princpalview/userprincpal.html',
        controller: 'userPrincpalController'
    })
	.state('principal.user.usermanagement', {
        url: '/adduser',
        templateUrl: 'princpalview/usermanagement.html',
        controller: 'usermanagementPrincpalController'
    })
	.state('principal.user.userrole', {
        url: '/role',
        templateUrl: 'princpalview/role.html',
        controller: 'userrolePrincpalController'
    })
	.state('principal.plan', {
        url: '/plan',
        templateUrl: 'princpalview/planprincpal.html',
        controller: 'princpalPlan'
    })
	.state('principal.plan.timetable', {
        url: '/timetable',
        templateUrl: 'princpalview/timeTable.html',
        controller: 'princpalPlanTimetableController'
    })
	.state('principal.plan.facultytimetable', {
        url: '/facultytimetable',
        templateUrl: 'princpalview/facultytimeTable.html',
        controller: 'princpalPlanFacultyTimetableController'
    })
	.state('principal.plan.facultyplan', {
        url: '/facultyplan',
        templateUrl: 'princpalview/plan.html',
        controller: 'princpalPlanController'
    })
	.state('principal.plan.facultywds', {
        url: '/facultywds',
        templateUrl: 'princpalview/wds.html',
        controller: 'princpalwdsController'
    })
	<!-------------- PRINCIPAL END ------------------->
	
	
	
	
	
	.state('hod',{
		url: '/hod',
		templateUrl: 'hodview/hod.html',
		controller: 'hodHomeController'
	})
	.state('hod.dept',{
		url: '/dept',
		templateUrl: 'hodview/dept.html',
		controller: 'hodDepartmentController'
	})
	.state('hod.course',{
		url: '/course',
		 templateUrl: 'homeview/course.html',
        controller: 'coursecontroller'
	})
	.state('hod.subject',{
		url: '/subject',
		templateUrl: 'homeview/subject.html',
        controller: 'subjectcontroller'
	})
	.state('hod.user',{
		url: '/user',
		templateUrl: 'homeview/usermangement.html',
        controller: 'homemanagementController'
	})
	.state('hod.timeTable',{
		url: '/timeTable',
		templateUrl: 'hodview/timeTable.html',
		controller: 'hodTimeTableController'
	})
	.state('hod.facultytime',{
		url: '/timeTable',
		templateUrl: 'hodview/facultytimeTable.html',
		controller: 'facultyTimeTableController'
	})
	.state('hod.plan',{
		url: '/plan',
		templateUrl: 'hodview/plan.html',
		controller: 'hodPlanController'
	})
	.state('hod.wds',{
		url: '/wds',
		templateUrl: 'homeview/wds.html',
        controller: 'wdsController'
	})
	.state('hod.profile',{
		url: '/profile',
		templateUrl: 'hodview/profile.html',
        controller: 'hodprofileController'
	})
	/*$locationProvider.html5Mode({
      enabled: true,
      requireBase: true
    });	*/
})

/*Admin.config(function($routeProvider){
	$routeProvider
	.when('/',{
		templateUrl: 'dashboardview/login.html',
		controller: 'loginController'
	})
	.when('/dashboard',{
		templateUrl: 'dashboardview/dashboard.html',
		controller: 'dashboardController'
	})
	.when('/profile',{
		templateUrl: 'dashboardview/profile.html',
		controller: 'profileController'
	})
	.when('/dept',{
		templateUrl: 'dashboardview/department.html',
		controller: 'deptController'
	})
	.when('/princpal',{
		templateUrl: 'dashboardview/princpal.html',
		controller: 'princpalController'
	})
	.when('/dept_head',{
		templateUrl: 'dashboardview/depthead.html',
		controller: 'headController'
	});
})*/