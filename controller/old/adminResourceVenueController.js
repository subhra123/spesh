var resourseclass=angular.module('Channabasavashwara');
resourseclass.controller('adminResourceVenueController',function($scope,$http,$state,$window,venueField){
	$scope.buttonName="Add";
	var id='';
	$scope.listOfCollege=[{
		name:'Select college',
		value:''
	}]
	$scope.colg_name=$scope.listOfCollege[0];
	$http({
		method:'GET',
		url:"php/department/readCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfCollege.push(data);
		});
	},function errorCallback(response) {
	});
	$http({
		method:'GET',
		url:"php/resource/readVenueData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.venueData=response.data;
	},function errorCallback(response) {
	});
	$scope.addSessionData=function(){
		if($scope.buttonName=="Add"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				venueField.borderColor('colg_name');
			}else if($scope.venue==null || $scope.venue==''){
				alert('Please add venue name...');
				venueField.borderColor('resourcevenue');
			}else{
				var userdata={'colg_name':$scope.colg_name.value,'venue':$scope.venue};
				$http({
					method:'POST',
					url:"php/resource/addVenueData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$scope.venue=null;
					//$state.go('dashboard.res.venue',{}, { reload: true });
					if($scope.venueData=='null'){
						$scope.venueData=[];
						$scope.venueData.push(response.data);
					}else{
						$scope.venueData.unshift(response.data);
					}
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.venue==response.data.venue_name){
						venueField.borderColor('resourcevenue');
					}else{
						//$state.go('dashboard.res.venue',{}, { reload: true });
					}
				});
			}
		}
		if($scope.buttonName=="Update"){
			if($scope.colg_name.value=='' || $scope.colg_name.value==null){
				alert('select college');
				venueField.borderColor('colg_name');
			}else if($scope.venue==null || $scope.venue==''){
				alert('Please add venue name...');
				venueField.borderColor('resourcevenue');
			}else{
				var updatedata={'colg_name':$scope.colg_name.value,'venue':$scope.venue,'id':id};
				$http({
					method:'POST',
					url:"php/resource/UpdateVenueData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.res.venue',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					if($scope.venue==response.data.venue_name){
						venueField.borderColor('resourcevenue');
					}else{
					//$state.go('dashboard.res.venue',{}, { reload: true });
					}
				});
			}
		}
	}
	$scope.editSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/resource/editVenueData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.colg_name.value=response.data[0].clg_id;
			$scope.venue=response.data[0].venue_name;
			$scope.buttonName="Update";
			$scope.cancelbuttonName="Cancel";
			$scope.colgread=true;
			$scope.colgdis=true;
			$scope.showCancel=true;
		},function errorCallback(response) {
		});
	}
	$scope.deleteSessionData=function(sid){
		id=sid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Venue');
		if(conmesg){
		$http({
			method:'POST',
			url:"php/resource/deleteVenueData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			alert(response.data['msg']);
			$state.go('dashboard.res.venue',{}, { reload: true });
		},function errorCallback(response) {
			alert(response.data['msg']);
			//$state.go('dashboard.res.venue',{}, { reload: true });
		});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			venueField.clearBorderColor(id);
		}
		
	}

	$scope.clearField=function(id){
		venueField.clearBorderColor(id);
	}
	$scope.cancelSessionData=function(){
		$state.go('dashboard.res.venue',{}, { reload: true });
	}
	$scope.listOfSearchCollege=[];
	$http({
		method:'GET',
		url:"php/stream/getCollegeData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.colg_name,'value':obj.profile_id};
			$scope.listOfSearchCollege.push(data);
		});
	},function errorCallback(response) {
	});
});
resourseclass.factory('venueField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#cccccc";
				 }
			});
		}
	};
});