var dashboard=angular.module('Channabasavashwara');
dashboard.controller('adminResController',function($scope,$http,$state,$window){
	$scope.tabs = {
    1: ($state.current.name === 'dashboard.res.userrole'),
    2: ($state.current.name === 'dashboard.res.class'),
    3: ($state.current.name === 'dashboard.res.section'),
	4: ($state.current.name === 'dashboard.res.session'),
	5: ($state.current.name === 'dashboard.res.unit'),
	6: ($state.current.name === 'dashboard.res.venue'),
	7: ($state.current.name === 'dashboard.res.day'),
	8: ($state.current.name === 'dashboard.res.time'),
	9: ($state.current.name === 'dashboard.res.lecturePlan')
    };
});