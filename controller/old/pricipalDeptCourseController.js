var resoursecourse=angular.module('Channabasavashwara');
resoursecourse.controller('pricipalDeptCourseController',function($scope,$http,$state,$window,courseField){
	
	$scope.buttonName="Add";
	var id='';
	$scope.stream_readonly= false;
	$scope.clearButtonName= "Cancel";
	$scope.showCancel =  false;
	
	$scope.listOfStream=null;
	$scope.listOfStream=[{
		name:'Select Stream',
		value:''
	}]
	$scope.stream_name=$scope.listOfStream[0];	
	
	$scope.listOfDept=null;
	$scope.listOfDept=[{
		name:'Select Department',
		value:''
	}]
	$scope.dept_name=$scope.listOfDept[0];
	
		
	 $http({
		method: 'GET',
		url:"php/stream/readCollegeStreamData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		angular.forEach(response.data,function(obj){
			var data={'name':obj.stream_name,'value':obj.stream_id};
			$scope.listOfStream.push(data);
		});
	},function errorCallback(response) {
		
	});
	 
	 
	
	 
	 
	
	$http({
		method:'GET',
		url:"php/course/readCollegeCourseData.php",
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		//console.log('res',response);
		$scope.courseData=response.data;
	},function errorCallback(response) {
	});
	
	
	$scope.addCourseData=function(){
		
		if($scope.buttonName=="Add"){
			
			if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('Please select Stream');
				courseField.borderColor('stream_name');
			}else if($scope.course_name==null || $scope.course_name==''){
				alert('Course name field could not be blank');
				courseField.borderColor('resourcecoursename');
			}else if($scope.short_name==null || $scope.short_name==''){
				alert('Course code field could not be blank');
				courseField.borderColor('resourceshortname');
			}else if($scope.semester==null || $scope.semester==''){
				alert('Please select the  Semester');
				courseField.borderColor('resourcesem');
			}else{
				//alert("::"+$scope.semester);
				var userdata={'stream_id':$scope.stream_name.value,'course_name':$scope.course_name,'short_name':$scope.short_name,'semester':$scope.semester};
				$http({
					method:'POST',
					url:"php/course/addCollegeCourseData.php",
					data:userdata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					//$state.go('principal.dept.course',{}, { reload: true });
					$scope.course_name=null;
					$scope.short_name=null;
					$scope.semester='';
					if($scope.courseData=='null'){
						$scope.courseData=[];
						$scope.courseData.push(response.data);
					}else{
						$scope.courseData.unshift(response.data);
					}
					
					},function errorCallback(response) {
					alert(response.data['msg']);
					//$state.go('dashboard.res.course',{}, { reload: true });
				})
			}
		}
		if($scope.buttonName=="Update"){
			//console.log('update',$scope.colg_name.value);
			if($scope.stream_name.value=='' || $scope.stream_name.value==null){
				alert('Please select Stream');
				courseField.borderColor('stream_name');
			}else if($scope.course_name==null || $scope.course_name==''){
				alert('Course Name field could not be blank');
				courseField.borderColor('resourcecoursename');
			}else if($scope.short_name==null || $scope.short_name==''){
				alert('Course Code field could not be blank');
				courseField.borderColor('resourceshortname');
			}else if($scope.semester==null || $scope.semester==''){
				alert('Please select the semester');
				courseField.borderColor('resourcesem');
			}else{
				//alert("Update");
				var updatedata={'stream_id':$scope.stream_name.value,'course_name':$scope.course_name,'short_name':$scope.short_name,'semester':$scope.semester,'id':id};
				$http({
					method:'POST',
					url:"php/course/updateCollegeCourseData.php",
					data:updatedata,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('principal.dept.course',{}, { reload: true });
				},function errorCallback(response) {
					alert(response.data['msg']);
					//$state.go('dashboard.res.course',{}, { reload: true });
				})
			}
		}
	}
	
	
	$scope.editCourseData=function(cid){
		id=cid;
		var userid={'id':id};
		$http({
			method:'POST',
			url:"php/course/editCourseData.php",
			data:userid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			//alert("::"+response.data[0].stream_id);
			$scope.stream_name.value=response.data[0].stream_id;
			$scope.course_name=response.data[0].course_name;
			$scope.short_name=response.data[0].short_name;
			$scope.semester=response.data[0].semester;
			$scope.buttonName="Update";
			$scope.stream_readonly= true;
			$scope.showCancel= true;
			
		},function errorCallback(response) {
		});
	}
	
	$scope.clearSubjectData=function(){
		$state.go('principal.dept.course',{}, { reload: true });	
	}
	
	
	$scope.deleteRoleData=function(cid){
		id=cid;
		var userid={'id':id};
		var conmesg=$window.confirm('Are you sure to remove this Course');
		if(conmesg){
			$http({
				method:'POST',
				url:"php/resource/deleteCourseData.php",
				data:userid,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('principal.dept.course',{}, { reload: true });
			},function errorCallback(response) {
				alert(response.data['msg']);
				//$state.go('dashboard.res.course',{}, { reload: true });
			});
		}
	}
	$scope.removeBorder=function(id,mod){
		//console.log('sc',mod);
		if(mod.value!= ''){
			courseField.clearBorderColor(id);
		}
	}
	$scope.clearField=function(id){
		courseField.clearBorderColor(id);
	}
});
resoursecourse.factory('courseField',function($timeout,$window){
	return{
		borderColor:function(id){
			 $timeout(function() {
				 var element = $window.document.getElementById(id);
				 if(element){
					  element.focus();
					  element.style.borderColor = "red";
				 }
			 });
		},
		clearBorderColor:function(id){
			$timeout(function() {
				var element = $window.document.getElementById(id);
				 if(element){
					 element.style.borderColor = "#CCCCCC";
				 }
			});
		}
	};
});