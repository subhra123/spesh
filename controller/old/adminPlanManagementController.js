var dept=angular.module('Channabasavashwara');
dept.controller('adminPlanManagementController',function($scope,$http,$state,$window){
	$scope.adminPlanTabs = {
		1: ($state.current.name === 'dashboard.plan.timetable'),
        2: ($state.current.name === 'dashboard.plan.facultytimetable'),
		3: ($state.current.name === 'dashboard.plan.facultyplan'),
        4: ($state.current.name === 'dashboard.plan.facwds')
	}
});