var city=angular.module('Spesh');
dept.controller('adminCityController',function($scope,$http,$state,$window){
	var id='';
	$scope.submitbuttonName='Add';
	$scope.submitButton=true;
	$http({
		method:'GET',
		url:'php/category/city.php?action=disp',
		headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	}).then(function successCallback(response){
		$scope.objCityData=response.data;
	},function errorCallback(response) {
	})
	$scope.addCity=function(billdata){
		if($scope.submitbuttonName=='Add'){
			if($scope.cityname==null || $scope.cityname==''){
				alert('Please add the city name');
			}else{
				var addData=$.param({'action':'add','cityname':$scope.cityname});
				$http({
					method:'POST',
					url:"php/category/city.php",
					data:addData,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.category.city',{},{reload:true});
				},function errorCallback(response) {
					alert(response.data['msg']);
				})
			}
		}
		if($scope.submitbuttonName=='Update'){
			if($scope.cityname==null || $scope.cityname==''){
				alert('Please add the city name');
			}else{
				var addData=$.param({'action':'update','cityname':$scope.cityname,'city_id':id});
				$http({
					method:'POST',
					url:"php/category/city.php",
					data:addData,
					headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
				}).then(function successCallback(response){
					alert(response.data['msg']);
					$state.go('dashboard.category.city',{},{reload:true});
				},function errorCallback(response) {
					alert(response.data['msg']);
				})
			}
		}
	}
	$scope.cancelUpdate =function(){
		$state.go('dashboard.category.city',{},{reload:true});
	}
	$scope.editCityData=function(cityid){
		id=cityid;
		var editid=$.param({'action':'edit','city_id':cityid});
		$http({
			method:'POST',
			url:"php/category/city.php",
			data:editid,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		}).then(function successCallback(response){
			$scope.cityname=response.data[0].city_name;
			$scope.cancelButton=true;
	 	    $scope.submitbuttonName="Update";
		},function errorCallback(response) {
		});
	}
	$scope.removeCityData=function(cityid){
		if(confirm("Are you sure want to delete city?")){
			var deletedata=$.param({'action':'delete','city_id':cityid});
			$http({
			method:'POST',
			url:"php/category/city.php",
			data:deletedata,
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			}).then(function successCallback(response){
				alert(response.data['msg']);
				$state.go('dashboard.category.city',{},{reload:true});
			},function errorCallback(response) {
				$state.go('dashboard.category.city',{},{reload:true});
			});
		}
	}
})